const INITIAL_STATE = {
    Edital_tipo: [], Edital_tipo_selecionado: [], Edital_pessoa_juridica: [], Edital_pessoa_juridica_selecionada: [], Edital_processo: [], Edital_Descricao: [], Edital_files: [], Edital_pond: null, Edital_sucess: false,
    Edital_palavras_chaves: [], Edital_Titulo: '', loading: false, errorEdital: '', sucessEdital: '', editalView: [{flagView:false,id:null}],warningEdital:'',
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'GET_TIPO':
            return { ...state, Edital_tipo: action.payload }
        case "SET_LOADING":
            console.log(action.payload)
            return { ...state, loading: action.payload }
        case 'GET_TIPO_SELECIONADO':
            return { ...state, Edital_tipo_selecionado: action.payload }
        case 'ADD_INPUT':
            return { ...state, Edital_palavras_chaves: [...state.Edital_palavras_chaves, action.payload] }
        case 'GET_PESSOA_JURIDICA':
            return { ...state, Edital_pessoa_juridica: action.payload }
        case 'GET_PESSOA_JURIDICA_SELECIONADA':
            return { ...state, Edital_pessoa_juridica_selecionada: action.payload }
        case 'GET_PROCESSO_SELECIONADO':
            return { ...state, Edital_processo: action.payload }
        case 'SET_TITULO_EDITAL':
            return { ...state, Edital_Titulo: action.payload }
        case 'GET_DESCRICAO_SELECIONADA':
            return { ...state, Edital_Descricao: action.payload }
        case 'GET_PALAVRAS_CHAVES':
            return { ...state, Edital_palavras_chaves: action.payload }
        case 'GET_FILE_NAMES':
            return { ...state, Edital_files: action.payload }
        case 'GET_POND':
            return { ...state, Edital_pond: action.payload }
        case 'GET_ERROS':
            return { ...state, errorEdital: action.payload }
        case 'GET_SUCESS_MENSAGER_EDITAL':
            return { ...state, sucessEdital: action.payload }
        case 'GET_ERROR_MENSAGER_EDITAL':
            return { ...state, errorEdital: action.payload }
        case 'GET_WARNING_MENSAGER_EDITAL':
            return {...state,warningEdital: action.payload}   
        case 'SET_VIEW_EDITAL':
            return { ...state, editalView: action.payload }
        case 'REMOVE_INPUT':
            return {
                ...state,
                Edital_palavras_chaves: state.Edital_palavras_chaves.filter((item, index) => index !== action.payload)
            }
        case "RESET":
            console.log("reset")
            return {
                ...INITIAL_STATE
            }
        case "SHOW_ALERT":
            return { ...state, Edital_sucess: action.payload }
        default:
            return state
    }
}               