import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col, Form, Button, } from 'react-bootstrap';
import {
    getTipo, changeTipo, getPessoaJuridica, changePessoaJuridica, changeProcesso,
    changeDescricao, addFileItem, addDocument, changePond, showAlert, TraingDocument, addInput,
    changePalavraChave, deleteInput, setTela, changeTitulo, setSucessData, setErrorData, enableEdit, setWarningData,setHeader
} from './editalActions'
import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import { getUrl } from '../../Constantes'
import { faTimes, faPlus, faEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './Style.css';
import Loading from '../loading/loading'
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';



class Edital extends Component {
    UNSAFE_componentWillMount() {
        this.props.changeTipo(1)
        this.props.setHeader("rowHeader")

    }

    renderPalavrasChaves() {
        console.log(this.props.editalView[0])
        return (
            <div className="rowPalavasChaves">
                <section className="containerPalavraChave flex flex-wrap">

                    {this.props.Edital_palavras_chaves.map((item, index) => (
                        <div key={index} className="box">
                            <div>
                                <p className="textoPalavraChave">{item.text}</p></div>
                            {this.props.editalView[0].flagView === false ?
                                <div className="divBtn"><Button onClick={() => this.props.deleteInput(index)} className="btnClose" variant="outline-primary"><FontAwesomeIcon className="iconClose" icon={faTimes} /></Button></div>
                                : null}
                        </div>
                    ))}
                </section>
            </div>
        )
    }
    add(event) {
        if (event.key === "Enter") {
            this.props.addInput(this.props.Edital_palavras_chaves, this.refs.inputPalavraChave.value)
            this.refs.inputPalavraChave.value = ''
        }
    }
    addPalavraChave() {
        this.props.addInput(this.props.Edital_palavras_chaves, this.refs.inputPalavraChave.value)
        this.refs.inputPalavraChave.value = ''
    }

    getMessenger() {
        if (this.props.errorEdital && this.props.errorEdital !== '') {
            NotificationManager.error(this.props.errorEdital, 'Erro :(', 3000)
            this.props.setErrorData('')
        }
        if (this.props.sucessEdital && this.props.sucessEdital !== '') {
            console.log("entrou sucess edital")
            NotificationManager.success(this.props.sucessEdital, 'Successo :)', 3000)
            this.props.setSucessData('')
        }
        if (this.props.warningEdital && this.props.warningEdital !== '') {
            console.log("entrou sucess edital")
            NotificationManager.info(this.props.warningEdital, 'Dados preenchidos incorretamente. :|', 3000)
            this.props.setWarningData('')
        }
    }
    changeProcesso(e) {
        console.log(e.key)
        const re = /^[0-9\b]+$/
        if (re.test(e.target.value) || e.target.value === '') {
            this.props.changeProcesso(e.target.value)
        }
    }
    render() {
        console.disableYellowBox = true;
        return (
            <div>
                {this.props.loading === true ? <Loading /> : null}
                {this.getMessenger()}
                <NotificationContainer />
                <div className="DivEdital">
                <Row >
                    <Col className="ColleftBack" md={4} sm={12} xs={12}>
                        <button onClick={() => this.props.editalView[0].flagView ? this.props.setTela("/lista", this.props): this.props.setTela("/paneledital", this.props)} class="btn-font btn-font-back  btn-font-sep-back fa-chevron-left  ">Voltar</button>
                    </Col>
                    <Col className="ColleftBack" md={8} sm={12} xs={12}> </Col>

                </Row>
                <Row>
                    <Col className="ColleftTitle" md={6} sm={6} xs={6}>
                            <h4 className='titleEdital'>INSERIR EDITAL</h4>
                    </Col>
                    <Col md={6} sm={6} xs={6}>
                                    {this.props.editalView[0].flagView === true ?
                                        <div className="tituloRight">
                                            <Button onClick={() => this.props.enableEdit(this.props.editalView[0].id)} className="btnPalavraChave" size="lg" variant="outline-primary"
                                            ><FontAwesomeIcon icon={faEdit}></FontAwesomeIcon></Button>
                                        </div>
                                        : null}
                                </Col>
                </Row>
                    <div className="divInterna">
                        <Form.Group controlId="formBasicEmail">

                            <Row className="RowElements">
                                <Col sm={12} md={3} lg={3}>
                                    <Form.Label className='textLabel'>Número do processo:</Form.Label>
                                    <Form.Control ref="inputNumeroProcesso" readOnly={this.props.editalView[0].flagView} className="inputText" onChange={(e) => this.changeProcesso(e)} value={this.props.Edital_processo} type="text" placeholder="Digite o número do processo" />
                                </Col>
                                <Col sm={12} md={9} lg={9}>
                                    <Form.Label className='textLabel'>Título:</Form.Label>
                                    <Form.Control className="inputText" readOnly={this.props.editalView[0].flagView} onChange={(e) => this.props.changeTitulo(e.target.value)} value={this.props.Edital_Titulo} type="text" placeholder="Digite o título" />
                                </Col>
                            </Row>
                            <Row className="RowElements">
                                <Col xs={12}>
                                    <Form.Label className='textLabel'>Descrição:</Form.Label>
                                    <Form.Control as="textarea" readOnly={this.props.editalView[0].flagView} className="textDescricao" onChange={(e) => this.props.changeDescricao(e.target.value)} value={this.props.Edital_Descricao} placeholder="Digite a descrição" />
                                </Col>
                            </Row>
                            {this.props.Edital_palavras_chaves.length > 0 ?
                                this.renderPalavrasChaves()
                                : null}
                            <Row>
                                <Col className="rowPalavasChaves" xs={4}>
                                    <Form.Label className='textLabel'>Palavra chave:</Form.Label>
                                    <div className="divRowPalavraChave">
                                        <Form.Control readOnly={this.props.editalView[0].flagView} ref="inputPalavraChave" onKeyPress={(event) => this.add(event, this.props.Edital_palavras_chaves, this.refs.inputPalavraChave.value)} className="inputText" type="text" placeholder={"Palavras chaves"} />
                                        <Button onClick={() => this.addPalavraChave()} className="btnPalavraChave" size="sm" variant="outline-primary"
                                        ><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                                    </div>
                                </Col>
                            </Row>
                            <Row className="filePond">
                                {console.log(JSON.parse(window.sessionStorage.getItem('dataUsers')).token)}
                                <div className="filePondDiv">
                                    <Col sm={12} xs={12} lg={12} md={12}>
                                        <Form.Label className='textLabel'>Upload de arquivos:</Form.Label>
                                        <FilePond
                                            allowMultiple={true}
                                            ref={ref => this.pond = ref}
                                            onupdatefiles={(fileItens) => {
                                                this.props.addFileItem(fileItens)
                                                this.props.changePond(this.pond)
                                            }}
                                            onremovefile={(err, file) => {
                                                console.log(file)
                                            }}
                                            instantUpload={true}
                                            files={this.props.Edital_files}
                                            disabled={this.props.editalView[0].flagView}
                                            labelIdle='Arraste e solte seus arquivos ou navegue.'
                                            server={
                                                {
                                                    url: getUrl(window.location.hostname).url_file_server,
                                                    process: {
                                                        headers: {
                                                            'Authorization': 'Bearer' + ' ' + JSON.parse(window.sessionStorage.getItem('dataUsers')).token
                                                        },
                                                    },
                                                    load: {
                                                        headers: {
                                                            'Authorization': 'Bearer' + ' ' + JSON.parse(window.sessionStorage.getItem('dataUsers')).token
                                                        },
                                                    }

                                                }


                                            }>
                                        </FilePond>
                                    </Col>
                                </div>

                            </Row>
                            {this.props.editalView[0].flagView === false ?
                                <Row className="rowSubmit">
                                    <div >
                                        <Button className="BtnSubmit" onClick={() => this.props.addDocument(this.props)} variant="primary"  >
                                            Salvar
                                    </Button>
                                    </div>

                                </Row>
                                : null}
                        </Form.Group>
                        <Row className="controlMargin">
                        </Row>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        Edital_tipo: state.Edital_tipo.Edital_tipo,
        Edital_tipo_selecionado: state.Edital_tipo_selecionado.Edital_tipo_selecionado,
        Edital_pessoa_juridica: state.Edital_pessoa_juridica.Edital_pessoa_juridica,
        Edital_pessoa_juridica_selecionada: state.Edital_pessoa_juridica_selecionada.Edital_pessoa_juridica_selecionada,
        Edital_processo: state.Edital_processo.Edital_processo,
        Edital_Descricao: state.Edital_Descricao.Edital_Descricao,
        Edital_files: state.Edital_files.Edital_files,
        Edital_pond: state.Edital_pond.Edital_pond,
        Edital_sucess: state.Edital_sucess.Edital_sucess,
        Edital_palavras_chaves: state.Edital_palavras_chaves.Edital_palavras_chaves,
        Edital_Titulo: state.Edital_Titulo.Edital_Titulo,
        loading: state.loading.loading,
        sucessEdital: state.sucessEdital.sucessEdital,
        errorEdital: state.errorEdital.errorEdital,
        editalView: state.editalView.editalView,
        warningEdital: state.warningEdital.warningEdital
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getTipo, changeTipo, getPessoaJuridica, changePessoaJuridica,
        changeProcesso, changeDescricao, addFileItem,
        addDocument, changePond, showAlert, TraingDocument,
        addInput, changePalavraChave, deleteInput, setTela, changeTitulo, setSucessData, setErrorData, enableEdit, setWarningData,setHeader
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Edital)