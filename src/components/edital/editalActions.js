import axios from 'axios';
import moment from 'moment';
import { getUrl } from '../../Constantes'
import { push } from 'connected-react-router'
const base_url = getUrl(window.location.hostname).base_url
const base_url_training = getUrl(window.location.hostname).base_url_training


export const addTicketData = (data) => {
    return {
        type: 'GET_TIPO',
        payload: data
    }
};


export const setPalavraChave = (data) => {
    return {
        type: 'SET_PALAVRA_CHAVE',
        payload: data
    }
}
export function changePalavraChave(index, texto) {
    let array = {
        index: index,
        texto: texto
    }
    return dispatch => {
        dispatch(setPalavraChave(array))
    }
}

export const addInputData = (data) => {
    return {
        type: 'ADD_INPUT',
        payload: data
    }
}

export function addInput(props, text) {
    let array = {
        id: props.length,
        text: text
    }
    return (dispatch) => {
        dispatch(addInputData(array))
    }
}

export const removeInputData = (data) => {
    return {
        type: 'REMOVE_INPUT',
        payload: data
    }
}

export function deleteInput(id) {
    console.log(id)
    return (dispatch) => {
        dispatch(removeInputData(id))
    }
}

export function getTipo() {
    return dispatch => {
        axios.get(base_url + "tipoDocumento/1").then(
            resp => {
                dispatch(addTicketData(resp.data)
                )
            }
        )

    }

}
export function changeTipo(e) {
    return dispatch => {
        axios.get(base_url + "tipoDocumento/" + e).then(
            resp => {
                dispatch(addTipoSelecionado(resp.data))
            })
    }
}
export function changePessoaJuridica(e) {
    return dispatch => {
        axios.get(base_url + "pessoaJuridica/" + e.target.value).then(
            resp => {
                dispatch(addPessoaJuridicaSelecionada(resp.data))
            })
    }
}
export const addPessoaJuridicaSelecionada = (data) => {
    return {
        type: 'GET_PESSOA_JURIDICA_SELECIONADA',
        payload: data
    }
};

export function changeProcesso(e) {
    return dispatch => {
        dispatch(addProcessoSelecionado(e))
    }
}
export const addProcessoSelecionado = (data) => {
    return {
        type: 'GET_PROCESSO_SELECIONADO',
        payload: data
    }
};

export const addTituloSelecionado = (data) => {
    return {
        type: 'SET_TITULO_EDITAL',
        payload: data
    }
}

export function changeTitulo(e) {
    return dispatch => {
        dispatch(addTituloSelecionado(e))
    }
}

export function changeDescricao(e) {
    return dispatch => {
        dispatch(addDescricaoSelecionada(e))
    }
}
export const addDescricaoSelecionada = (data) => {
    return {
        type: 'GET_DESCRICAO_SELECIONADA',
        payload: data
    }
};


export const addTipoSelecionado = (data) => {
    return {
        type: 'GET_TIPO_SELECIONADO',
        payload: data
    }
};

export const addPessoaJuridicaData = (data) => {
    return {
        type: 'GET_PESSOA_JURIDICA',
        payload: data
    }
};

export function getPessoaJuridica() {
    return dispatch => {
        axios.get(base_url + "pessoaJuridica").then(
            resp => {
                dispatch(addPessoaJuridicaData(resp.data)
                )
            }
        )

    }

}
export const resetState = (data) => {
    console.log(data)
    if (data.Edital_pond) {
        data.Edital_pond._pond.removeFiles()
    }
    return {
        type: 'RESET',
        payload: data
    }
};


function postDocument(element, data) {
    return dispatch => {
        axios.post(base_url + 'documento', element).then(
            resp => {
                //dispatch(processaDocumento(data, resp.data))
                let mensager = "dados enviados com sucesso"
                dispatch(resetState(data))
                dispatch(setSucessData(mensager))
                dispatch(setLoading(false))
            }

        ).catch(function (error) {
            dispatch(setErrorData("Houve um erro ao gravar os dados."))
            dispatch(setLoading(false))
        });
    }
}

export const showAlert_data = (data) => {
    return {
        type: 'SHOW_ALERT',
        payload: data
    }
};

export function showAlert(data) {
    return dispatch => {
        dispatch(showAlert_data(data))
    }
}

export const setSucessData = (data) => {
    return {
        type: 'GET_SUCESS_MENSAGER_EDITAL',
        payload: data
    }
};

export const setErrorData = (data) => {
    return {
        type: 'GET_ERROR_MENSAGER_EDITAL',
        payload: data
    }
};
export const setWarningData = (data) => {
    return {
        type: 'GET_WARNING_MENSAGER_EDITAL',
        payload: data
    }
};

function processaDocumento(data, respdata) {
    return dispatch => {
        axios.get(base_url_training + 'leitura').then(
            resp => {
                
                    if(resp.data.success === false){
                        dispatch(disableDocument(respdata,"Documento não atualizado devido a caracteres especiais."))
                    }else{
                        console.log(data)
                        let mensager = "dados enviados com sucesso"
                        dispatch(resetState(data))
                        dispatch(setSucessData(mensager))
                        dispatch(setLoading(false))
                }
                }

        ).catch(function (error) {
                    dispatch(disableDocument(respdata,"Houve um erro ao gravar os dados."))

                });
    }
}

export function disableDocument(data,errorMensager) {
    data.documentoAtivo = "0"
    return dispatch => {
        axios.post(base_url + 'documento', data).then(
            resp => {
                dispatch(setErrorData(errorMensager))
                dispatch(setLoading(false))
            }

        ).catch(function (error) {
            dispatch(setErrorData(errorMensager))
            dispatch(setLoading(false))
        });
    }
}


export const setLoading = (data) => {
    return (dispatch) => {
        dispatch({
            type: 'SET_LOADING',
            payload: data
        })
    }
};


export function addDocument(data) {
    console.log(data)
    return dispatch => {
        dispatch(setLoading(true));
        let palavrasChaves = ''
        let filename = []
        data.Edital_palavras_chaves.forEach(element => {
            if (palavrasChaves === '') {
                palavrasChaves = element.text
            }
            else {
                palavrasChaves = palavrasChaves + "," + element.text
            }
        })
        data.Edital_files.forEach(element => {
            filename.push(element.filename)
        });
        filename = JSON.stringify(filename)
        filename = filename.replace(']', '')
        filename = filename.replace('[', '')
        var RegExp = /["|']/g;
        filename = filename.replace(RegExp, "");
        const deteTime = moment().format("DD/MM/YYYY hh:mm:ss")
        let _Data = []
        console.log(data.editalView)
        if (data.editalView.length > 0 && data.editalView[0].flagView === false && data.editalView[0].id != null) {
            _Data = {
                documentoId: data.editalView[0].id,
                pessoaJuridica: {
                    "pessoaJuridicaId": 1,
                    "tipoPessoaJuridica": {
                        "tipoPessoaJuridicaId": 1,
                        "tipoPessoaJuridicaDescricao": "PJ",
                        "tipoPessoaJuridicaAtivo": "1"
                    }
                },
                documentoPai: null,
                tipoDocumento: data.Edital_tipo_selecionado,
                titulo: data.Edital_Titulo,
                documentoProcessadoLeitura: '0',
                documentosDescricao: data.Edital_Descricao,
                documentosDataCadastro: deteTime,
                corpo: "",
                nrProcesso: data.Edital_processo,
                documentosArquivos: filename,
                documentosPalavrasChaves: palavrasChaves,
                documentoAtivo: '1'
            }
        }
        else {
            _Data = {
                pessoaJuridica: {
                    "pessoaJuridicaId": 1,
                    "tipoPessoaJuridica": {
                        "tipoPessoaJuridicaId": 1,
                        "tipoPessoaJuridicaDescricao": "PJ",
                        "tipoPessoaJuridicaAtivo": "1"
                    }
                },
                documentoPai: null,
                tipoDocumento: data.Edital_tipo_selecionado,
                titulo: data.Edital_Titulo,
                documentosDescricao: data.Edital_Descricao,
                documentosDataCadastro: deteTime,
                documentoProcessadoLeitura: '0',
                corpo: "",
                nrProcesso: data.Edital_processo,
                documentosArquivos: filename,
                documentosPalavrasChaves: palavrasChaves,
                documentoAtivo: '1'
            }
        }

        if (_Data.documentosArquivos !== "" && _Data.titulo !== "" && _Data.nrProcesso !== []) {
            dispatch(postDocument(_Data, data))
        } else {
            dispatch(setLoading(false))
            dispatch(setWarningData("Por favor preencha os dados obrigatórios."))
        }



    }

}

export function TraingDocument() {
    axios.get(base_url_training + 'processa_documentos').then(
        resp => {
            console.log(resp.data)
        }

    ).catch(function (error) {
        console.log(error)
    });
}


export const addFileData = (data) => {
    console.log(data)
    return {
        type: 'GET_FILE_NAMES',
        payload: data
    }
};

export function addFileItem(item) {
    return dispatch => {
        dispatch(addFileData(item))
    }
}

export const addPondData = (data) => {
    return {
        type: 'GET_POND',
        payload: data
    }
}

export function changePond(pond) {
    console.log(pond)
    return dispatch => {
        dispatch(addPondData(pond))
    }
}

export const setTela = (url, data) => {
    return dispatch => {
        dispatch(push(url));
        dispatch(resetState(data));
    };
}
export function enableEdit(id) {
    return dispatch => {
        dispatch(addViewData(false, id))
    }
}
export function addViewData(flag, id) {
    const data = [{
        flagView: flag,
        id: id
    }]
    return {
        type: 'SET_VIEW_EDITAL',
        payload: data
    }
}
export const setHeader = (data) => {
    return {
        type: 'SET_CLASS_HEADER_BACKGROUND',
        payload: data
    }
  }
  