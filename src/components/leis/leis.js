import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col, Form, Button, } from 'react-bootstrap';
import { saveTermos, setTela, setErrorData, setSucessData, getTermos, deleteInput, ChangeDescricao, ChangePalavraChave, deleteInputTermosAdicionados, gravaDados, setHeader } from './leisActions'
import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import { getUrl } from '../../Constantes'
import { faSearch, faTimes, faAngleRight, faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import btBack from '../img/bt-voltar-01b.png'
import './Style.css';
import Popup from "reactjs-popup"
import Loading from '../loading/loading'
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';



class Termos extends Component {
    componentDidMount() {
        this.props.setHeader("rowHeader")
    }
    getMessenger() {
        if (this.props.errorEdital && this.props.errorEdital !== '') {
            NotificationManager.error(this.props.errorEdital, 'Erro :(', 3000)
            this.props.setErrorData('')
        }
        if (this.props.sucessEdital && this.props.sucessEdital !== '') {
            console.log("entrou sucess edital")
            NotificationManager.success(this.props.sucessEdital, 'Successo :)', 3000)
            this.refs.nomeLei.value = ''
            this.refs.linkLei.value = ''
            this.refs.descricaoLei.value = ''
            this.props.setSucessData('')
        }
        if (this.props.warningEdital && this.props.warningEdital !== '') {
            console.log("entrou sucess edital")
            NotificationManager.info(this.props.warningEdital, 'Dados preenchidos incorretamente. :|', 3000)
            this.props.setWarningData('')
        }
    }
    render() {

        return (
            <div>
                <div className="divTermo">
                    {this.props.loading === true ? <Loading /> : null}
                    {this.getMessenger()}
                    <NotificationContainer />
                    {console.log(this.props)}
                    <Row >
                        <Col className="ColleftBack" md={4} sm={12} xs={12}>
                            <button onClick={() => this.props.setTela("/panel", this.props)} class="btn-font btn-font-back  btn-font-sep-back fa-chevron-left  ">Voltar</button>
                        </Col>
                        <Col className="ColleftTitle" md={4} sm={12} xs={12}>
                            <div className="divTitleEdital">
                                <h4 className='titleEdital'>Leis</h4>
                            </div>

                        </Col>
                        <Col className="ColleftBack" md={4} sm={12} xs={12}> </Col>

                    </Row>
                    <div className="divInterna">
                        <Form.Group controlId="formBasicEmail">
                            <Row className="RowElements">
                                <Col xs={12} sm={12} md={12} lg={12}>
                                    <Form.Label className='textLabel'>Nome:</Form.Label>
                                    <Form.Control ref="nomeLei" className="inputText" type="text" placeholder="Digite o termo que deseja pesquisar" />
                                </Col>
                            </Row>
                            <Row className="RowElements">
                                <Col xs={12} sm={12} md={12} lg={12}>
                                    <Form.Label className='textLabel'>Link:</Form.Label>
                                    <Form.Control ref="linkLei" className="inputText" type="text" placeholder="Digite o termo que deseja pesquisar" />
                                </Col>
                            </Row>
                            <Row className="RowElements">
                                <Col xs={12}>
                                    <Form.Label className='textLabel'>Descrição:</Form.Label>
                                    <Form.Control as="textarea" ref="descricaoLei" className="textDescricao" placeholder="Digite a descrição" />
                                </Col>
                            </Row>
                            <Row className="rowSubmit">
                                <div >
                                    <Button className="BtnSubmit" onClick={() => this.props.gravaDados(this.refs.nomeLei.value, this.refs.linkLei.value, this.refs.descricaoLei.value)} variant="primary"  >
                                        Salvar
                                    </Button>
                                </div>

                            </Row>
                        </Form.Group>


                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        loading: state.loading.loading,
        listaTermos: state.listaTermos.listaTermos,
        errorEdital: state.errorEdital.errorEdital,
        sucessEdital: state.sucessEdital.sucessEdital,
        listaTermosAdicionados: state.listaTermosAdicionados.listaTermosAdicionados,
        inputPalavraChavePopup: state.inputPalavraChavePopup.inputPalavraChavePopup,
        inputDescricaoPopup: state.inputDescricaoPopup.inputDescricaoPopup
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setTela, gravaDados, setSucessData, setHeader
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Termos)