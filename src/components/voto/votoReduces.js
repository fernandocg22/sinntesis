const INITIAL_STATE = { Voto_tipo: [], Voto_tipo_selecionado: [], Voto_pessoa_juridica: [], Voto_pessoa_juridica_selecionada: [], Voto_processo: [], Voto_Descricao: [],Voto_files:[],Voto_pond:[],Voto_sucess:false }
export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'GET_TIPO':
            console.log("pegou o tipo")
            return { ...state, Voto_tipo: action.payload }
        case 'GET_TIPO_SELECIONADO':
            return { ...state, Voto_tipo_selecionado: action.payload }
        case 'GET_PESSOA_JURIDICA':
            console.log("pegou pessoa juridica")
            return { ...state, Voto_pessoa_juridica: action.payload }
        case 'GET_PESSOA_JURIDICA_SELECIONADA':
            return { ...state, Voto_pessoa_juridica_selecionada: action.payload }
        case 'GET_PROCESSO_SELECIONADO':
            return { ...state, Voto_processo: action.payload }
        case 'GET_DESCRICAO_SELECIONADA':
            return { ...state, Voto_Descricao: action.payload }
        case 'GET_FILE_NAMES':
            return { ...state, Voto_files: action.payload }
        case 'GET_POND':
            return { ...state, Voto_pond: action.payload }   
        case "RESET":
            console.log("reset")
            return {...state, Voto_tipo: [],
                 Voto_tipo_selecionado: [],
                 Voto_pessoa_juridica: [],
                 Voto_pessoa_juridica_selecionada: [],
                 Voto_processo: [],
                 Voto_Descricao: [],
                 Voto_files:[],
                 Voto_pond:[],
                 Voto_sucess:true }  
        case "SHOW_ALERT":
            return {...state, Voto_sucess: action.payload }         
        default:
            return state
    }
}               