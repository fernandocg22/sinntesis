import axios from 'axios';
import moment from 'moment';
import {base_url} from '../../Constantes'
import {base_url_training} from '../../Constantes'



export const addTicketData = (data) => {
    return {
        type: 'GET_TIPO',
        payload: data
    }
};

export function getTipo() {
    return dispatch => {
        axios.get(base_url + "tipoDocumento").then(
            resp => {
                dispatch(addTicketData(resp.data)
                )
            }
        )

    }

}
export function changeTipo(e) {
    console.log(e.target.value)
    return dispatch => {
        axios.get(base_url + "tipoDocumento/" + e.target.value).then(
            resp => {
                dispatch(addTipoSelecionado(resp.data))
            })
    }
}
export function changePessoaJuridica(e) {
    return dispatch => {
        axios.get(base_url + "pessoaJuridica/" + e.target.value).then(
            resp => {
                dispatch(addPessoaJuridicaSelecionada(resp.data))
            })
    }
}
export const addPessoaJuridicaSelecionada = (data) => {
    return {
        type: 'GET_PESSOA_JURIDICA_SELECIONADA',
        payload: data
    }
};

export function changeProcesso(e) {
    return dispatch => {
        dispatch(addProcessoSelecionado(e.target.value))
    }
}
export const addProcessoSelecionado = (data) => {
    return {
        type: 'GET_PROCESSO_SELECIONADO',
        payload: data
    }
};

export function changeDescricao(e) {
    return dispatch => {
        dispatch(addDescricaoSelecionada(e.target.value))
    }
}
export const addDescricaoSelecionada = (data) => {
    return {
        type: 'GET_DESCRICAO_SELECIONADA',
        payload: data
    }
};


export const addTipoSelecionado = (data) => {
    return {
        type: 'GET_TIPO_SELECIONADO',
        payload: data
    }
};

export const addPessoaJuridicaData = (data) => {
    return {
        type: 'GET_PESSOA_JURIDICA',
        payload: data
    }
};

export function getPessoaJuridica() {
    return dispatch => {
        axios.get(base_url + "pessoaJuridica").then(
            resp => {
                dispatch(addPessoaJuridicaData(resp.data)
                )
            }
        )

    }

}
export const resetState = (data) => {
    return {
        type: 'RESET',
        payload: data
    }
};


function postDocument(data) {
          return axios.post(base_url + 'catalogoDocumentos', data).then(
                resp => {
                  return resp.data
                }

            ).catch(function (error) {
                console.log(error)
        });
}

export const showAlert_data = (data) => {
    return {
        type: 'SHOW_ALERT',
        payload: data
    }
};

export function showAlert(data) {
    return dispatch => {
        dispatch(showAlert_data(data))
    }
}


export function addDocument(data) {
    return dispatch => {
        const _Datas = []
        data.Voto_files.forEach(element => {
            const deteTime = moment().format("DD/MM/YYYY hh:mm:ss")
            const _Data = {
                pessoaJuridica: data.Voto_pessoa_juridica_selecionada,
                tipoDocumento: data.Voto_tipo_selecionado,
                catalogoDocumentosUrl: element.filename,
                catalogoDocumentosTitulo: data.Voto_Descricao,
                catalogoDocumentosDataCadastro: deteTime,
                catalogoDocumentosNrProcesso: data.Voto_processo,
                catalogoDocumentosFilename: element.filename,
                catalogoDocumentosProcessado: "0"
            }
            _Datas.push(_Data)
        });
        var retorno = false
        _Datas.forEach(element => {
            postDocument(element).then(data => {
                retorno = data
                console.log(retorno)
            });
        });


        //console.log(retorno)
        dispatch(resetState())
        data.Voto_pond._pond.removeFiles()
        dispatch(getTipo())
        dispatch(getPessoaJuridica())
        console.log(retorno)
    }

}

export function TraingDocument() {
            axios.get(base_url_training + 'processa_documentos').then(
                resp => {
                    console.log(resp.data)
                }

            ).catch(function (error) {
                console.log(error)
        });
    }


export const addFileData = (data) => {
    console.log(data)
    return {
        type: 'GET_FILE_NAMES',
        payload: data
    }
};

export function addFileItem(item) {
    return dispatch => {
        dispatch(addFileData(item))
    }
}

export const addPondData = (data) => {
    return {
        type: 'GET_POND',
        payload: data
    }
}

export function changePond(pond) {
    console.log(pond)
    return dispatch => {
        dispatch(addPondData(pond))
    }
}
export const setHeader = (data) => {
    return {
        type: 'SET_CLASS_HEADER_BACKGROUND',
        payload: data
    }
  }
  