import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Container, Row, Col, Form, Button,Body,Alert} from 'react-bootstrap';
import { getTipo, changeTipo, getPessoaJuridica, changePessoaJuridica, changeProcesso, changeDescricao, addFileItem, addDocument, changePond,showAlert,TraingDocument,setHeader } from './votoActions'
import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import './Style.css';
import {url_file_server} from '../../Constantes'

class Voto extends Component {
    UNSAFE_componentWillMount() {
        this.props.getTipo()
        this.props.getPessoaJuridica()
        this.props.setHeader("rowHeader")
    }
    render() {
        return (
            <div className="Div">
            <Row className="RowElements">
                <Col xs={5}>
                    <h4 className='titleVoto'>Voto</h4>
                    <p className='textVoto'>Faça aqui o upload de votos, e indique para qual decisão este voto está sendo cadastrado.
                    </p>
                
                </Col>
            </Row>
                <Row className="RowElements">
                    {console.log(this.props)}
                    <Col xs={6}>
                        <Form.Group controlId="exampleForm.ControlSelect1">
                            <Form.Control  className="inputText"
                                onChange={(e) => this.props.changeTipo(e)}
                                as="select">
                                <option defaultValue="" >Selecione o tipo</option>
                                {this.props.Voto_tipo ?

                                    this.props.Voto_tipo.map((item, index) => {
                                        return (
                                            <option key={index} value={JSON.stringify(item.tipoDocumentoId)}>{item.tipoDocumentoDescricao}</option>
                                        )
                                    }) : null
                                }
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col xs={6}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Control className="inputText" onChange={(e) => this.props.changeDescricao(e)} value={this.props.Voto_Descricao} type="email" placeholder="Descrição" />
                        </Form.Group>
                    </Col>
                </Row>
                <Row className="RowElements">
                <Col xs={6}>
                        <Form.Group controlId="exampleForm.ControlSelect1">
                            <Form.Control  className="inputText"
                                onChange={(e) => this.props.changePessoaJuridica(e)}
                                as="select">
                                <option defaultValue="" >Selecione a pessoa juridica</option>
                                {this.props.Voto_pessoa_juridica ?

                                    this.props.Voto_pessoa_juridica.map((item, index) => {
                                        return (
                                            <option key={index} value={JSON.stringify(item.pessoaJuridicaId)}>{item.pessoaJuridicaId}</option>
                                        )
                                    }) : null
                                }
                            </Form.Control>
                        </Form.Group>
                    </Col>

                <Col xs={6}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Control className="inputText" onChange={(e) => this.props.changeProcesso(e)} value={this.props.Voto_processo} type="email" placeholder="Número do processo" />
                        </Form.Group>
                    </Col>

                </Row>
                <Row>
                    <Col><FilePond
                        allowMultiple={true}
                        ref={ref => this.pond = ref}
                        onupdatefiles={(fileItens) => {
                            this.props.addFileItem(fileItens)
                            this.props.changePond(this.pond)
                        }}
                        labelIdle='Arraste e solte seus arquivos ou Navegue'
                        server={url_file_server} /></Col>

                </Row>
                <Row>
                    <Col  md={{ span: 4, offset: 9 }}>
                    <Button className="BtnSubmit" onClick={() => this.props.addDocument(this.props)} variant="primary"  >
                            Salvar
                        </Button>
                        <Button className="BtnSubmit" onClick={() => TraingDocument()} variant="primary"  >
                            Processar
                        </Button>
                    </Col>
                </Row>
                <Row className="controlMargin">
                </Row>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        Voto_tipo: state.Voto_tipo.Voto_tipo,
        Voto_tipo_selecionado: state.Voto_tipo_selecionado.Voto_tipo_selecionado,
        Voto_pessoa_juridica: state.Voto_pessoa_juridica.Voto_pessoa_juridica,
        Voto_pessoa_juridica_selecionada: state.Voto_pessoa_juridica_selecionada.Voto_pessoa_juridica_selecionada,
        Voto_processo: state.Voto_processo.Voto_processo,
        Voto_Descricao: state.Voto_Descricao.Voto_Descricao,
        Voto_files: state.Voto_files.Voto_files,
        Voto_pond: state.Voto_pond.Voto_pond,
        Voto_sucess: state.Voto_sucess.Voto_sucess,


    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getTipo, changeTipo, getPessoaJuridica, changePessoaJuridica, changeProcesso, changeDescricao, addFileItem, addDocument, changePond,showAlert,TraingDocument,setHeader}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Voto)