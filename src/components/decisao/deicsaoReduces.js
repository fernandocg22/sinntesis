import { text } from "@fortawesome/fontawesome-svg-core"
import { routerActions } from "connected-react-router"

const INITIAL_STATE = {
    Decisao_representacao_data: [], Decisao_tipo_selecionado: [], Decisao_pessoa_juridica: [], Decisao_pessoa_juridica_selecionada: [], Decisao_processo: [], Decisao_Descricao: [], Decisao_files: [], Decisao_pond: null, Decisao_sucess: false,

    Decisao_palavras_chaves: [], Decisao_titulo: '', loading_decisao: false, errorDecisao: '', sucessDecisao: '', warningDecisao: '', Decisao_Texto: '', generatePdf: false
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'GET_REPRESENTACAO':
            return { ...state, Decisao_representacao_data: action.payload }
        case "SET_LOADING_DECISAO":
            return { ...state, loading_decisao: action.payload }
        case 'GET_TIPO_SELECIONADO':
            return { ...state, Decisao_tipo_selecionado: action.payload }
        case 'ADD_INPUT_DECISAO':
            return { ...state, Decisao_palavras_chaves: [...state.Decisao_palavras_chaves, action.payload] }
        case 'GET_PESSOA_JURIDICA':
            return { ...state, Decisao_pessoa_juridica: action.payload }
        case 'GET_PESSOA_JURIDICA_SELECIONADA':
            return { ...state, Decisao_pessoa_juridica_selecionada: action.payload }
        case 'GET_PROCESSO_SELECIONADO':
            return { ...state, Decisao_processo: action.payload }
        case 'GET_DESCRICAO_DECISAO_SELECIONADA':
            return { ...state, Decisao_Descricao: action.payload }
        case 'GET_FILE_NAMES_DECISAO':
            return { ...state, Decisao_files: action.payload }
        case 'SET_TITULO_DECISAO':
            return { ...state, Decisao_titulo: action.payload }
        case 'GET_POND_DECISAO':
            return { ...state, Decisao_pond: action.payload }
        case 'GET_REPRESENTACAO_SELECIONADO':
            console.log(action.payload)
            return { ...state, Decisao_representacao_selecionada: action.payload }
        case 'GET_SUCESS_MENSAGER_DECISAO':
            return { ...state, sucessDecisao: action.payload }
        case 'SET_FLAG_PDF':
            return { ...state, generatePdf: action.payload }
        case 'GET_ERROR_MENSAGER_DECISAO':
            return { ...state, errorDecisao: action.payload }
        case 'GET_WARNING_MENSAGER_DECISAO':
            return { ...state, warningDecisao: action.payload }
        case 'REMOVE_INPUT':
            return {
                ...state,
                Decisao_palavras_chaves: state.Decisao_palavras_chaves.filter((item, index) => index !== action.payload)
            }
        case "RESET_DECISAO":
            console.log("reset")
            return {
                ...state, Decisao_tipo: [],
                Decisao_tipo_selecionado: [],
                Decisao_representacao_data: [],
                Decisao_pessoa_juridica: [],
                Decisao_pessoa_juridica_selecionada: [],
                Decisao_processo: [],
                Decisao_Descricao: [],
                Decisao_files: [],
                Decisao_pond: null,
                Decisao_sucess: true,
                Decisao_titulo: '',
                Decisao_palavras_chaves: [],
                errorDecisao: '',
                sucessDecisao: ''

            }
        case "SHOW_ALERT":
            return { ...state, Decisao_sucess: action.payload }
        case "GET_TEXT_DECISAO_DATA":
            return { ...state, Decisao_Texto: action.payload }
        default:
            return state
    }
}               