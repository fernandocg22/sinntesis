import axios from 'axios';
import moment from 'moment';
import { getUrl } from '../../Constantes'
import { push } from 'connected-react-router'
const base_url = getUrl(window.location.hostname).base_url
const base_url_training = getUrl(window.location.hostname).base_url_training



export const setPalavraChave = (data) => {
    return {
        type: 'SET_PALAVRA_CHAVE',
        payload: data
    }
}
export function changePalavraChave(index, texto) {
    let array = {
        index: index,
        texto: texto
    }
    return dispatch => {
        dispatch(setPalavraChave(array))
    }
}

export const addInputData = (data) => {
    return {
        type: 'ADD_INPUT_DECISAO',
        payload: data
    }
}

export function addInput(props, text) {
    let array = {
        id: props.length,
        text: text
    }
    return (dispatch) => {
        dispatch(addInputData(array))
    }
}

export const removeInputData = (data) => {
    return {
        type: 'REMOVE_INPUT',
        payload: data
    }
}

export function deleteInput(id) {
    console.log(id)
    return (dispatch) => {
        dispatch(removeInputData(id))
    }
}

export function getRepresentacao() {
    return dispatch => {
        axios.get(base_url + "documento/2").then(
            resp => {
                dispatch(addRepresentacaoData(resp.data)
                )
            }
        )

    }

}

export const addTituloSelecionado = (data) => {
    return {
        type: 'SET_TITULO_DECISAO',
        payload: data
    }
}

export function changeTitulo(e) {
    return dispatch => {
        dispatch(addTituloSelecionado(e.target.value))
    }
}


export const addRepresentacaoData = (data) => {
    return {
        type: 'GET_REPRESENTACAO',
        payload: data
    }
};

export const changeRepresentacao = (e) => {
    return {
        type: 'GET_REPRESENTACAO_SELECIONADO',
        payload: e[0]
    }
}

export function changePessoaJuridica(e) {
    return dispatch => {
        axios.get(base_url + "pessoaJuridica/" + e.target.value).then(
            resp => {
                dispatch(addPessoaJuridicaSelecionada(resp.data))
            })
    }
}
export const addPessoaJuridicaSelecionada = (data) => {
    return {
        type: 'GET_PESSOA_JURIDICA_SELECIONADA',
        payload: data
    }
};

export function changeProcesso(e) {
    return dispatch => {
        dispatch(addProcessoSelecionado(e.target.value))
    }
}
export const addProcessoSelecionado = (data) => {
    return {
        type: 'GET_PROCESSO_SELECIONADO',
        payload: data
    }
};

export function changeDescricao(e) {
    return dispatch => {
        dispatch(addDescricaoSelecionada(e.target.value))
    }
}
export const addDescricaoSelecionada = (data) => {
    return {
        type: 'GET_DESCRICAO_DECISAO_SELECIONADA',
        payload: data
    }
};


export const addTipoSelecionado = (data) => {
    return {
        type: 'GET_TIPO_SELECIONADO',
        payload: data
    }
};

export const addPessoaJuridicaData = (data) => {
    return {
        type: 'GET_PESSOA_JURIDICA',
        payload: data
    }
};

export function getPessoaJuridica() {
    return dispatch => {
        axios.get(base_url + "pessoaJuridica").then(
            resp => {
                dispatch(addPessoaJuridicaData(resp.data)
                )
            }
        )

    }

}
export const resetState = (data) => {

    if(data.Decisao_pond){
        console.log("pond")
        data.Decisao_pond._pond.removeFiles()
    }
    return {
        type: 'RESET_DECISAO',
        payload: data
    }
};


function postDocument(element,data) {
    console.log(data)
    return dispatch => {
        axios.post(base_url + 'documento', element).then(
            resp => {
                //dispatch(processaDocumento(data,resp.data))
                let mensager = "dados enviados com sucesso"
                dispatch(getRepresentacao())
                dispatch(setSucessData(mensager))
                dispatch(setLoading(false))
                dispatch(resetState(data))
            }

        ).catch(function (error) {
            dispatch(setErrorData("Houve um erro ao gravar os dados."))
            dispatch(setLoading(false))
        });
    }
}


export const showAlert_data = (data) => {
    return {
        type: 'SHOW_ALERT',
        payload: data
    }
};

export function showAlert(data) {
    return dispatch => {
        dispatch(showAlert_data(data))
    }
}

export const setLoading = (data) => {
    return (dispatch) => {
        dispatch({
            type: 'SET_LOADING_DECISAO',
            payload: data
        })
    }
};

export const setSucessData = (data) => {
    return {
        type: 'GET_SUCESS_MENSAGER_DECISAO',
        payload: data
    }
};

export const setErrorData = (data) => {
    return {
        type: 'GET_ERROR_MENSAGER_DECISAO',
        payload: data
    }
};

function processaDocumento(data,respdata) {
    return dispatch => {
        axios.get(base_url_training + 'leitura').then(
            resp => {
                if(resp.data.success === false){
                    dispatch(disableDocument(respdata,"Documento não atualizado devido a caracteres especiais."))
                }else{
                let mensager = "dados enviados com sucesso"
                dispatch(getRepresentacao())
                dispatch(setSucessData(mensager))
                dispatch(setLoading(false))
                dispatch(resetState(data))
                }
            }

        ).catch(function (error) {
            dispatch(disableDocument(respdata,"Houve um erro ao gravar os dados."))

        });
    }
}

export function disableDocument(data,errorMensager){
    data.documentoAtivo = "0"
    return dispatch => {
        axios.post(base_url + 'documento', data).then(
            resp => {
                dispatch(setErrorData(errorMensager))
                dispatch(setLoading(false))
            }

        ).catch(function (error) {
            dispatch(setErrorData(errorMensager))
            dispatch(setLoading(false))
        });
    }
}

export function addDocument(data) {
    console.log(data)
    return dispatch => {
        let palavrasChaves = ''
        let filename = []
        data.Decisao_palavras_chaves.forEach(element => {
            if (palavrasChaves === '') {
                palavrasChaves = element.text
            }
            else {
                palavrasChaves = palavrasChaves + "," + element.text
            }
        })
        data.Decisao_files.forEach(element => {
            filename.push(element.filename)
        });
        filename = JSON.stringify(filename)
        filename = filename.replace(']', '')
        filename = filename.replace('[', '')
        var RegExp = /["|']/g;
        filename=filename.replace(RegExp,"");
        if(!data.Decisao_representacao_selecionada){
            dispatch(setWarningData("Por favor selecione um edital."))
        }
        else{
        const deteTime = moment().format("DD/MM/YYYY hh:mm:ss")
            const _Data = {
                pessoaJuridica: {
                    "pessoaJuridicaId": 1,
                    "tipoPessoaJuridica": {
                        "tipoPessoaJuridicaId": 1,
                        "tipoPessoaJuridicaDescricao": "PJ",
                        "tipoPessoaJuridicaAtivo": "1"
                    }
                },
                tipoDocumento: {
                    "tipoDocumentoId": 3,
                    "tipoDocumentoDescricao": "decisão",
                    "tipoDocumentoAtivo": "1"
                },
                titulo: data.Decisao_titulo,
                corpo:'',
                documentosDescricao: data.Decisao_Descricao,
                documentosDataCadastro: deteTime,
                documentosArquivos: filename,
                documentoProcessadoLeitura: '0',
                documentosPalavrasChaves: palavrasChaves,
                documentoPai: data.Decisao_representacao_selecionada,
                nrProcesso: data.Decisao_representacao_selecionada.nrProcesso,
                documentoAtivo: "1"

            }

            if(_Data.documentosArquivos===""){
                dispatch(setWarningData("Por favor adicione pelo menos um arquivo."))
            }else if(_Data.titulo===""){
                dispatch(setWarningData("Por favor preencha o titulo."))
             }
             else{
                 dispatch(setLoading(true));
                dispatch(postDocument(_Data,data))
             }
            }
    }

}

export function TraingDocument() {
    axios.get(base_url_training + 'processa_documentos').then(
        resp => {
            console.log(resp.data)
        }

    ).catch(function (error) {
        console.log(error)
    });
}


export const addFileData = (data) => {
    console.log(data)
    return {
        type: 'GET_FILE_NAMES_DECISAO',
        payload: data
    }
};

export function addFileItem(item) {
    return dispatch => {
        dispatch(addFileData(item))
    }
}

export const addPondData = (data) => {
    return {
        type: 'GET_POND_DECISAO',
        payload: data
    }
}

export function changePond(pond) {
    console.log(pond)
    return dispatch => {
        dispatch(addPondData(pond))
    }
}

export const setTela = (url,data) => {
    return dispatch => {
        dispatch(push(url));
        dispatch(resetState(data));
    };
}
export const setWarningData = (data) => {
    return {
        type: 'GET_WARNING_MENSAGER_DECISAO',
        payload: data
    }
};
export function generatePDf(data){
    return {
        type: 'SET_FLAG_PDF',
        payload: data
    }
}
export const setHeader = (data) => {
    return {
        type: 'SET_CLASS_HEADER_BACKGROUND',
        payload: data
    }
  }
  