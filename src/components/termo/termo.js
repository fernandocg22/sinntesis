import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col, Form, Button, } from 'react-bootstrap';
import { saveTermos, setTela, setErrorData, setSucessData, getTermos, deleteInput, ChangeDescricao, ChangePalavraChave, deleteInputTermosAdicionados, gravaDados, setHeader } from './termoActions'
import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import { getUrl } from '../../Constantes'
import { faSearch, faTimes, faAngleRight, faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import btBack from '../img/bt-voltar-01b.png'
import './Style.css';
import Popup from "reactjs-popup"
import Loading from '../loading/loading'
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';


class Termos extends Component {
    componentDidMount() {
        this.props.setHeader("rowHeader")
    }
    search(event, inputPesquisa) {
        console.log(event)
        if (event.key === "Enter") {
            if (inputPesquisa !== "") {
                this.props.getTermos(inputPesquisa)
            }
        }
    }
    getMessenger() {
        console.log("entrou aqui")
        if (this.props.errorEdital && this.props.errorEdital !== '') {
            NotificationManager.error(this.props.errorEdital, 'Erro :(', 3000)
            this.props.setErrorData('')
        }
        if (this.props.sucessEdital && this.props.sucessEdital !== '') {
            console.log("entrou sucess edital")
            NotificationManager.success(this.props.sucessEdital, 'Successo :)', 3000)
            this.props.setSucessData('')
        }
        if (this.props.warningEdital && this.props.warningEdital !== '') {
            console.log("entrou sucess edital")
            NotificationManager.info(this.props.warningEdital, 'Dados preenchidos incorretamente. :|', 3000)
            this.props.setWarningData('')
        }
    }
    renderPalavrasChaves() {
        return (
            <div className="rowPalavasChaves">
                <section className="containerPalavraChave flex flex-wrap">

                    {this.props.listaTermos.map((item, index) => (
                        <div key={index} className="box">
                            <div>
                                <p className="textoPalavraChave">{item.dicionarioTermosTermo}</p></div>
                            <div className="divBtn"><Button onClick={() => this.props.deleteInput(index, this.props.listaTermos)} className="btnClose" variant="outline-primary"><FontAwesomeIcon className="iconClose" icon={faPlus} /></Button></div>
                        </div>
                    ))}
                </section>
            </div>
        )
    }

    renderPalavrasChavesAdiconadas() {
        return (
            <div className="rowPalavasChaves">
                <p className='textLabel'>Palavas chaves adicionadas:</p>
                <section className="containerPalavraChave flex flex-wrap">

                    {this.props.listaTermosAdicionados.map((item, index) => (
                        <div key={index} className="box">
                            <div>
                                <p className="textoPalavraChave">{item.dicionarioTermosTermo}</p></div>
                            <div className="divBtn"><Button onClick={() => this.props.deleteInputTermosAdicionados(index, this.props.listaTermos)} className="btnClose" variant="outline-primary"><FontAwesomeIcon className="iconClose" icon={faMinus} /></Button></div>
                        </div>
                    ))}
                </section>
            </div>
        )
    }
    gravaDadosTermo(e) {
        console.log(e)
        this.props.gravaDados(this.props.listaTermosAdicionados)
        this.refs.inputPesquisa.value = ''

    }
    render() {

        return (
            <div>
                <div className="divTermo">
                    {this.props.loading === true ? <Loading /> : null}
                    {this.getMessenger()}
                    <NotificationContainer />
                    <Row >
                        <Col className="ColleftBack" md={4} sm={12} xs={12}>
                            <button onClick={() => this.props.setTela("/panel", this.props)} class="btn-font btn-font-back  btn-font-sep-back fa-chevron-left  ">Voltar</button>
                        </Col>
                        <Col className="ColleftTitle" md={4} sm={12} xs={12}>
                            <div className="divTitleEdital">
                                <h4 className='titleEdital'>Termos</h4>
                            </div>

                        </Col>
                        <Col className="ColleftBack" md={4} sm={12} xs={12}> </Col>

                    </Row>

                    <div className="divInterna">
                        <Form.Group controlId="formBasicEmail">
                            <Row className="RowElements">
                                <Col md={10} xs={10}>
                                    <Form.Label className='textLabel'>Pesquisa de termos</Form.Label>
                                    <Form.Control onKeyPress={(event) => this.search(event, this.refs.inputPesquisa.value)} ref="inputPesquisa" className="inputText" value={this.props.Edital_processo} type="text" placeholder="Digite o termo que deseja pesquisar" />
                                </Col>
                                <Col xs={1} md={1}>
                                    <Button onClick={() => this.props.getTermos(this.refs.inputPesquisa.value)} title="Buscar edital." className="BtnPesquisaTermos" variant="primary" type="submit">
                                        <FontAwesomeIcon icon={faSearch}
                                            size="lg">
                                        </FontAwesomeIcon>
                                    </Button>
                                </Col>
                                <Col xs={1} md={1}>
                                    <Popup modal className="popUp"
                                        trigger={
                                            <Button title="Inserir Palavras Chaves." className="BtnPlusKey" variant="primary" type="submit">
                                                <FontAwesomeIcon icon={faPlus}
                                                    size="lg">
                                                </FontAwesomeIcon>
                                            </Button>
                                        }
                                        position="right center">
                                        {close => (
                                            <div>
                                                <Button title="Sair." className="BtnLogoutModal" onClick={() => close()} variant="primary" >
                                                    <FontAwesomeIcon className="iconCloseModal" icon={faTimes}
                                                        size="lg">
                                                    </FontAwesomeIcon>
                                                </Button>
                                                <Form.Group controlId="formBasicEmail">
                                                    <Row className="marginInsert">
                                                        <Col md={12} lg={12}>
                                                            <Form.Label className='textLabel'>Termo:</Form.Label>
                                                            <Form.Control ref="inputTermo" readOnly={false} onChange={(e) => this.props.ChangePalavraChave(e.target.value)} className="inputText" type="text" placeholder="Digite o termo" />
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col md={12} lg={12}>
                                                            <Form.Label className='textLabel'>Descrição:</Form.Label>
                                                            <Form.Control ref="inputDescricao" as="textarea" onChange={(e) => this.props.ChangeDescricao(e.target.value)} readOnly={false} className="textDescricao" placeholder="Digite a descrição" />
                                                        </Col>
                                                    </Row>
                                                    <Row className="rowBtnGravar">
                                                        {console.log(this.props)}
                                                        <Col sm={2} lg={2}>
                                                            <Button className="BtnSubmit" onClick={() => { this.props.saveTermos(this.props.inputPalavraChavePopup, this.props.inputDescricaoPopup); close(); }} variant="primary"  >
                                                                Inserir
                                </Button>
                                                        </Col>
                                                    </Row>
                                                </Form.Group>
                                            </div>
                                        )}
                                    </Popup>
                                </Col>
                            </Row>
                            {this.props.listaTermos && this.props.listaTermos.length > 0 ?
                                this.renderPalavrasChaves()
                                : null}

                            {this.props.listaTermosAdicionados && this.props.listaTermosAdicionados.length > 0 ?
                                this.renderPalavrasChavesAdiconadas()
                                : null}
                            <Row className="rowBtnGravar">
                                {console.log(this.props)}
                                <Col sm={2} lg={2}>
                                    <Button title="Salvar Palavras Chaves." className="BtnSubmit" onClick={(e) => this.gravaDadosTermo(e)} variant="primary"  >
                                        Salvar
                                </Button>
                                </Col>
                            </Row>

                        </Form.Group>


                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        loading: state.loading.loading,
        listaTermos: state.listaTermos.listaTermos,
        errorEdital: state.errorEdital.errorEdital,
        sucessEdital: state.sucessEdital.sucessEdital,
        listaTermosAdicionados: state.listaTermosAdicionados.listaTermosAdicionados,
        inputPalavraChavePopup: state.inputPalavraChavePopup.inputPalavraChavePopup,
        inputDescricaoPopup: state.inputDescricaoPopup.inputDescricaoPopup
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        saveTermos, setTela, setErrorData, setSucessData, getTermos, deleteInput, ChangeDescricao, ChangePalavraChave, deleteInputTermosAdicionados, gravaDados, setHeader
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Termos)