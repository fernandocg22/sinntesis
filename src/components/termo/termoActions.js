import axios from 'axios';
import moment from 'moment';
import { getUrl } from '../../Constantes'
import { push } from 'connected-react-router'
const base_url = getUrl(window.location.hostname).base_url
const base_url_training = getUrl(window.location.hostname).base_url_training



export function gravaDados(data) {
    console.log(data)
    if (data.length === 0) {
        return dispatch => { dispatch(setLoading(false))
                             dispatch( setErrorData("Por favor insira um termo antes de Salvar."))
        }
    }
    else {
        return dispatch => {
            axios.post(base_url + 'dicionarioTermos/saveList', data).then(
                resp => {
                    console.log("aqui")
                    dispatch(clearTermos(null))
                    dispatch(setSucessData("Termo cadastrado com sucesso."))
                    dispatch(setLoading(false))
                }

            ).catch(function (error) {
                dispatch(setLoading(false))
                dispatch(setErrorData("Houve um erro ao deletar o termo"))
            });
        }
    }
}



export function saveTermos(termo, descricao) {
    return dispatch => {
        const Ngran = termo.split(" ").length
        const TermoPalavraChave = "1"
        const data = { "dicionarioTermosTermo": termo, "dicionarioTermosDescricao": descricao, "dicionarioTermosNGram": Ngran, "dicionarioTermosPalavraChave": TermoPalavraChave }
        dispatch(addTermoAdicionado(data))
    }
}
export const setLoading = (data) => {
    return (dispatch) => {
        dispatch({
            type: 'SET_LOADING_LOGIN',
            payload: data
        })
    }
};
export const setTela = (url) => {
    console.log(url)
    return dispatch => {
        dispatch(push(url));
    };
}
export const setErrorData = (data) => {
    return (dispatch) => {
        dispatch({
            type: 'GET_ERROR_MENSAGER_EDITAL',
            payload: data
        })
    }
};

export const setSucessData = (data) => {
    return {
        type: 'GET_SUCESS_MENSAGER_EDITAL',
        payload: data
    }
};
export function getTermos(data) {
    return dispatch => {
        axios.get(base_url + 'dicionarioTermos/like/' + data).then(
            resp => {
                dispatch(setTermosData(resp.data))
            }

        ).catch(function (error) {
            dispatch(setErrorData("Não foi possível buscar o termo indicado."))
        });
    }
}
export const setTermosData = (data) => {
    return {
        type: 'GET_TERMO_DATA',
        payload: data
    }
};
export const removeInputData = (data) => {
    return {
        type: 'REMOVE_INPUT_TERMOS',
        payload: data
    }
}

export function deleteInput(id, data) {
    console.log(id)
    return (dispatch) => {
        dispatch(removeInputData(id))
        dispatch(addTermoAdicionado(data[id]))
    }
}
export const removeInputDataAdicionado = (data) => {
    return {
        type: 'REMOVE_INPUT_TERMOS_ADICIONADOS',
        payload: data
    }
}
export function deleteInputTermosAdicionados(id, data) {
    console.log(id)
    return (dispatch) => {
        dispatch(removeInputDataAdicionado(id))
    }
}


export function addTermoAdicionado(data) {
    console.log(data)
    return (dispatch) => {
        dispatch(addTermoAdicionadoData(data))
    }
}
export const addTermoAdicionadoData = (data) => {
    return {
        type: 'ADD_TERMOS_ADICIONADOS',
        payload: data
    }
}
export const clearTermos = (data) => {
    return {
        type: 'CLEAR_ALL_TERMOS',
        payload: data
    }
}


export const ChangePalavraChave = (data) => {
    console.log(data)
    return {
        type: 'CHANGE_PALAVRA_CHAVE',
        payload: data
    }
};

export const ChangeDescricao = (data) => {
    return {
        type: 'CHANGE_DESCRICAO',
        payload: data
    }
};
export const setHeader = (data) => {
    return {
        type: 'SET_CLASS_HEADER_BACKGROUND',
        payload: data
    }
  }