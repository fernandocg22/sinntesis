const INITIAL_STATE = {
    StyleHeader: null
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'SET_CLASS_HEADER_BACKGROUND':
            return { ...state, StyleHeader: action.payload }
        default:
            return state
    }
}