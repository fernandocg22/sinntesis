import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-bootstrap';
import logoDataMine from '../img/logo-sinntesis.png';
import logoTCESP from '../img/logo-sinntesis-interna-2.png';
import logoSINNTESIS from '../img/logo-sinntesis.png'
import LoginComponent from './../login/loginComponent'
import './Style.css';

class Header extends Component {
    render() {
        return (
            <div>
                <Row className="rowHeader" >
                    <Col md={6} className='logoEdital' xs={6}>
                        <img alt="" className="imgEiditalView" src={logoTCESP} />
                    </Col>

                    <Col className="logoTce" xs={6} md={6}>
                        <LoginComponent />
                    </Col>
                </Row>



            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        StyleHeader: state.StyleHeader.StyleHeader
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)

