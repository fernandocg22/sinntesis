import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-bootstrap';
import logoTCESP from '../img/logo-sinntesis-interna-2.png';
import rodape from '../img/rodape.png'
import LoginComponent from './../login/loginComponent'
import './Style.css';

class Footer extends Component {
    render() {
        return (
            <div className="divFooter">
                <div className="textFooter"> <h5 className="textFooter" >Copyright © 2020 Todos os direitos reservados | Grupo MZ Participações | Sinn Tecnologia</h5></div>
                <div className="imgFooter"> <img className="rodapeImg" alt="" src={rodape} /></div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer)

