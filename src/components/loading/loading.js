import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'
import React, { Component } from 'react'
import { Container } from 'react-bootstrap';
import './Style.css';

class Loading extends Component {
    render() {
        return (
                <div className="divPrincipal">
                    <div className="divInternaLoading">
                        <LoadingOverlay
                            active={true}
                            spinner
                            text='Enviando dados pro sistema...'
                        >
                        </LoadingOverlay>
                    </div>
                </div>

        )
    }
}

export default Loading