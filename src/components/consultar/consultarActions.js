import axios from 'axios';
import { getUrl } from '../../Constantes'
import { push } from 'connected-react-router'
import { Document, Packer, Paragraph, TextRun } from 'docx';
import { saveAs } from 'file-saver';
const base_url = getUrl(window.location.hostname).base_url

export const addConsultarData = (data) => {
    return {
        type: 'GET_TIPO',
        payload: data
    }
};

export const setValueConsulta = (data) => {
    return {
        type: 'GET_VALUE_CONSULTA_EDITAL_DATA',
        payload: data
    }
};

export function getEditais(value) {
    console.log(value)
    return dispatch => {
        dispatch(setLoading(true))
        axios.get(base_url + "documento/buscaProcesso/" + value.toLowerCase()).then(
            resp => {
                dispatch(setValueConsulta(value))
                dispatch(addConsultarData(resp.data))
                dispatch(setLoading(false))
                
            }
        )

    }

}
export const setHeader = (data) => {
    return {
        type: 'SET_CLASS_HEADER_BACKGROUND',
        payload: data
    }
  }

export const setLoading = (data) => {
    return (dispatch) => {
        dispatch({
            type: 'SET_LOADING',
            payload: data
        })
    }
};

export const addTipoSelecionado = (data) => {
    return {
        type: 'GET_TIPO_SELECIONADO',
        payload: data
    }
};

export const addDocumentData = (data) => {
    return {
        type: 'GET_DOCUMENT',
        payload: data
    }
};

export const addProximidadeData = (data) => {
    return {
        type: 'GET_PROXIMIDADE',
        payload: data
    }
};

function trataDados(dados, d) {
    //console.table(dados) 
    console.log(d)
    return addProximidadeData(dados);
}

export function searchDocuments(data) {
    return dispatch => {
        axios.get(base_url + "proximidade/" + data.document_selecionado).then(
            resp => {
                dispatch(trataDados(resp.data, data.document_selecionado))
            }
        )

    }

}

export function addProximdidade(data) {
    return {
        type: 'GET_DOCUMENT_SELECIONADO',
        payload: data
    }
}

export function changeDocument(e) {
    //console.log(e.target.value)
    return dispatch => {
        dispatch(addProximdidade(e.target.value))
    }
}

export function changeFile(e) {
    return dispatch => {
        dispatch(addFile(e.target.value))
    }
}

export function addFile(data) {
    return {
        type: 'GET_FILE_SELECIONADO',
        payload: data
    }
}

export function changeTipo(e) {
    return dispatch => {
        dispatch(addTipoSelecionado(e.target.value))
        axios.get(base_url + "documento/" + e.target.value).then(
            resp => {
                dispatch(addDocumentData(resp.data))
            }
        )

    }
}

export const redirect = (url, data) => {
    return dispatch => {
        dispatch(changeEditalSelecionado(data))
        dispatch(changeRepresentacaoByEditalId(data, url));
    };
}
export const setTela = (url, data) => {
    return dispatch => {
        if (url === "/lista") {
            dispatch(changeRepresentacaoByEditalIdData(null))
            dispatch(push(url));
        }
        else {
            dispatch(push(url));
            dispatch(resetState(data));
        }
    };
}


export const resetState = (data) => {
    return {
        type: 'RESET_CONSULTAR',
        payload: data
    }
};

export const changeEditalSelecionado = (data) => {
    return {
        type: 'CHANGE_EDITAL_SELECIONADO',
        payload: data
    }
};

export const changeRepresentacaoByEditalIdData = (data) => {
    return {
        type: 'CHANGE_CONSULTAR_REPRESENTACAO',
        payload: data
    }
}

export function changeRepresentacaoByEditalId(edital, url) {
    return dispatch => {
        axios.get(base_url + "documento/buscaRepresentacoesDecisoes/" + edital.documentoId).then(
            resp => {
                dispatch(changeRepresentacaoByEditalIdData(resp.data))
                if (resp.data.length > 0)
                    dispatch(push(url));
                if (resp.data.length === 0) {
                    dispatch(setErrorData("Não há representação para este edital."))
                }

            }
        )

    }
}
export const setErrorData = (data) => {
    console.log(data)
    return {
        type: 'GET_ERROR_MENSAGE_CONSULTAR',
        payload: data
    }
};
export const setHendler = (data) => {
    console.log(data)
    return {
        type: 'SET_HENDLER',
        payload: data
    }
}
export function getEditaisProximos(data) {
    return dispatch => {
        axios.get(base_url + "proximidade/buscaRepresentacao/" + data.representacaoId).then(
            resp => {
                dispatch(getEditaisProximosPC(data))
                dispatch(getEditaisProximosData(resp.data))
                dispatch(setHendler(true))
                console.log(resp.data)
            }
        )

    }
}

export function getEditaisProximosPC(data, rowIndex) {
    return dispatch => {
        axios.get(base_url + "proximidade/buscaProximidadeRepresentacaoPalavrasChaves/" + data.representacaoId).then(
            resp => {
                dispatch(getEditaisProximosPalaChaveData(resp.data))
                console.log(resp.data)
            }
        )
    }
}

export function getEditaisProximosPalaChaveData(data) {
    return {
        type: 'CHANGE_EDITAIS_PROXIMOS_PC',
        payload: data
    }
}

export function getEditaisProximosData(data) {
    return {
        type: 'CHANGE_EDITAIS_PROXIMOS',
        payload: data
    }
}

export function clickIndex(data) {
    return {
        type: 'CHANGE_INDEX_CLICKED',
        payload: data
    }
}

export function getWordClound(data) {
    return dispatch => {
        axios.get(base_url + "ocorrencia/wordcloud/" + data).then(
            resp => {
                console.log(resp.data)
                dispatch(changeWordClound(resp.data))
            }
        )

    }
}

export function changeWordClound(data) {
    return {
        type: 'CHANGE_WORD_CLOUND',
        payload: data
    }
}
// get data to edit edital
export function editEdital(data) {
    return dispatch => {
        axios.get(base_url + "documento/buscaDocumento/" + data).then(
            resp => {
                console.log(resp.data)
                dispatch(addDescricaoSelecionada(resp.data.documentosDescricao))
                dispatch(addTituloSelecionado(resp.data.titulo))
                dispatch(addProcessoSelecionado(resp.data.nrProcesso))
                dispatch(setPalavraChave(resp.data.documentosPalavrasChaves))
                dispatch(setFiles(resp.data.documentosArquivos))
                dispatch(addViewData(true, resp.data.documentoId))
                dispatch(push('/edital'))
            }
        )

    }
}

export const addDescricaoSelecionada = (data) => {
    return {
        type: 'GET_DESCRICAO_SELECIONADA',
        payload: data
    }
};

export const addTituloSelecionado = (data) => {
    return {
        type: 'SET_TITULO_EDITAL',
        payload: data
    }
}

export const addProcessoSelecionado = (data) => {
    return {
        type: 'GET_PROCESSO_SELECIONADO',
        payload: data
    }
};

export const setPalavraChave = (data) => {
    return dispatch => {
        const stringData = data.split(",")
        const Data = []
        stringData.map((item) => {
            let array = {
                id: 0,
                text: item
            }
            Data.push(array)
        })
        dispatch(addPalavrasChaves(Data))
    }
}

export const addPalavrasChaves = (data) => {
    return {
        type: 'GET_PALAVRAS_CHAVES',
        payload: data
    }
};

export function addViewData(data, documentoId) {
    data = [{
        flagView: true,
        id: documentoId
    }]
    return {
        type: 'SET_VIEW_EDITAL',
        payload: data
    }
}

export const addFileData = (data) => {
    return {
        type: 'GET_FILE_NAMES',
        payload: data
    }
};

export function addFileItem(item) {
    return dispatch => {
        dispatch(addFileData(item))
    }
}
export function setFiles(data) {
    return dispatch => {
        const stringData = data.split(",")
        const Data = []
        stringData.map((item) => {
            let array = {
                source: item,
                options: {
                    type: 'local'
                }
            }
            return Data.push(array)
        })
        dispatch(addFileItem(Data))
    }
}

export function backToLista() {
    return dispatch => {
        dispatch(getEditaisProximosData(null))
        dispatch(getEditaisProximosPalaChaveData(null))
        dispatch(setTela('/lista'))
        dispatch(clickIndex(null))
    }
}
export const addTextDecisaoData = (data) => {
    return {
        type: 'GET_TEXT_DECISAO_DATA',
        payload: data
    }
};
export function openTextDecisao(id){
    console.log(id)
    return dispatch => {
        axios.get(base_url + "documento/buscaCorpo/"+id).then(
            resp => {
                console.log(JSON.stringify(resp.data.documentoCorpo))
                dispatch(addTextDecisaoData(resp.data.documentoCorpo))
                dispatch(push('/textoDecisao'))
            }
        )

    }
}
export function getTextDecisao(id){
    console.log(id)
    return dispatch => {
        axios.get(base_url + "documento/buscaCorpo/"+id).then(
            resp => {
                const doc = new Document();
                let lorem = resp.data.documentoCorpo 
                lorem = lorem.replace(/\f/g,"")  
                console.log(lorem)
                const paragraph = new Paragraph({
                    children: [new TextRun((lorem))],
                });
                doc.addSection({
                    properties: {},
                    children: [paragraph],
                });
                Packer.toBlob(doc).then(blob => {
                    console.log(blob);
                    saveAs(blob, "example.docx");
                    console.log("Document created successfully");
                });
                
            }
        )

    }
}

export const deleteEditallogico = (data,consultavalue) => {
    console.log(data)
    data['documentoAtivo'] = "0"
    console.log(data)
    return dispatch => {
        axios.post(base_url + 'documento', data).then(
            resp => {
                //dispatch(processaDocumento(data, resp.data))
                dispatch(getEditais(consultavalue))
            }

        ).catch(function (error) {
            console.log(error)
        });
    }
}