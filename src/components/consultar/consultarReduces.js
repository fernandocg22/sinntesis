const INITIAL_STATE = {
    tipo: null, documents: [], tipo_selecionado: [], proximidade: [], document_selecionado: [], editalSelecionado: [],
    listaRepresentacoes: null, listaEditaisProximos: null, clickedIndex: null, wordCloud: [],
    consultarError: '', consultarSucess: '', listaEditaisProximosPC: null,handleShow:false,consultaValue:null
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'GET_TIPO':
            return { ...state, tipo: action.payload }
        case 'GET_DOCUMENT':
            return { ...state, document: action.payload }
        case 'GET_TIPO_SELECIONADO':
            return { ...state, tipo_selecionado: action.payload }
        case 'GET_PROXIMIDADE':
            return { ...state, proximidade: action.payload }
        case 'GET_DOCUMENT_SELECIONADO':
            return { ...state, document_selecionado: action.payload }
        case 'CHANGE_CONSULTAR_REPRESENTACAO':
            return { ...state, listaRepresentacoes: action.payload }
        case 'CHANGE_EDITAL_SELECIONADO':
            return { ...state, editalSelecionado: action.payload }
        case 'SET_HENDLER':
            return { ...state, handleShow: action.payload }
        case 'CHANGE_EDITAIS_PROXIMOS':
            return { ...state, listaEditaisProximos: action.payload }
        case 'CHANGE_INDEX_CLICKED':
            return { ...state, clickedIndex: action.payload }
        case 'CHANGE_EDITAIS_PROXIMOS_PC':
            return { ...state, listaEditaisProximosPC: action.payload }
        case 'GET_ERROR_MENSAGE_CONSULTAR':
            return { ...state, consultarError: action.payload }
        case 'GET_VALUE_CONSULTA_EDITAL_DATA':
            return {...state, consultaValue: action.payload}
        case 'CHANGE_WORD_CLOUND':
            return { ...state, wordCloud: action.payload }
        case 'RESET_CONSULTAR':
            return { ...INITIAL_STATE }
        default:
            return state
    }
}
