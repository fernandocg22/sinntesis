
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col, Form, Button, Modal } from 'react-bootstrap';
import { faDownload, faAngleDown, faAngleUp, faColumns, faEdit, faTimes, faFileDownload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { redirect, getEditaisProximos, clickIndex, getWordClound, setErrorData, openTextDecisao, setHendler, getTextDecisao, setHeader } from "../consultar/consultarActions"
import './Style.css';
import { getUrl } from '../../Constantes'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import { TagCloud } from 'react-tagcloud'


import axios from 'axios';

const downloadDocuments = (row, rowIndex) => {
    console.log(row)
    if (!row || row === "") return null
    let src = row
    src = src.replace('}', '')
    src = src.replace('{', '')
    let srcs = src.split(',')

    srcs.forEach(element => {
        console.log(element)
        let url = getUrl(window.location.hostname).url_file_server + "download?object=" + element;
        axios({
            url: url,
            responseType: 'blob',
            method: 'GET',
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', element);
            document.body.appendChild(link);
            link.click();
        });
    })
}

class RepresentacaoTable extends Component {
    componentDidMount() {
        this.props.setHeader("rowHeader")
    }
    openTextDecisao(row) {

        this.props.openTextDecisao(row.filhoId)
    }
    downloadDocx(row) {
        this.props.getTextDecisao(row.filhoId)
    }
    getClassName() {
        if ((this.props.listaEditaisProximos && this.props.listaEditaisProximos.length > 0) || (this.props.listaEditaisProximosPC && this.props.listaEditaisProximosPC.length > 0)) {
            return 'tableConstutar50percent'
        }
        else { return 'tableConsultar' }
    }
    getCssProximidade() {
        if ((this.props.listaEditaisProximos && this.props.listaEditaisProximos.length > 0) || (this.props.listaEditaisProximosPC && this.props.listaEditaisProximosPC.length > 0)) {
            return "tableRight"
        }
        else {
            return "tableRightOpacity"
        }
    }
    formatHeader(column, colIndex, { sortElement, filterElement }) {
        return (
            <div>
                <h5 key={colIndex} className="headerTable">{column.text}{sortElement}</h5></div>
        );
    }
    formatHeaderProximos(column, colIndex, { sortElement, filterElement }) {
        return null
    }
    renderPalavrasChaves(palavras_chaves) {
        if (palavras_chaves !== "") {
            palavras_chaves = palavras_chaves.split(",")
            return (
                <div className="divPalavrasChavesInterna">
                    {this.props.wordCloud.length > 0 ?
                        <div className="divTermos">
                            <div> <p className="textDecisao"><b>Termos que mais ocorrem</b></p></div>
                            <hr />
                            <section className="consultarPalavraChave flex flex-wrap">

                                {this.props.wordCloud.map((item, index) => (

                                    <div>
                                        {console.log(item)}
                                        <div><p key={index} className="textPalavraChaveBox">{item.value}</p></div></div>
                                ))}
                            </section>
                        </div>
                        : null}
                    {this.props.palavras_chaves !== '' ?
                        <div className="divPalavrasChaves">
                            <div><p className="textDecisao"><b>Palavras chaves</b></p></div>
                            <hr />
                            <div className="rowPalavraChaveExpand">
                                <div className="rowPalavasChavesInterna">
                                    <section className="consultarPalavraChave flex flex-wrap">

                                        {palavras_chaves.map((item, index) => (

                                            <div>
                                                <div><p key={index} className="textPalavraChaveBox">{item}</p></div></div>
                                        ))}
                                    </section>
                                </div>
                            </div>
                        </div>
                        : null}
                </div>
            )
        }
    }
    columnsProximos = [
        {
            dataField: 'id',
            text: '',
            hidden: true,
        }, {
            dataField: 'processo',
            text: 'Número do processo',
            sort: true,
            classes: 'columnTable'
        }, {
            dataField: 'titulo',
            text: 'Título',
            sort: true,
            classes: 'columnTable'
        },
        {
            dataField: 'proximidadeSintatica',
            text: 'ST',
            sort: true,
            classes: 'columnTable',
            formatter: (cell, row, rowIndex) => {
                return (<b key={rowIndex}>{row.proximidadeSintatica}%</b>)
            }
        },
        {
            dataField: 'proximidadeSemantica',
            text: 'SM',
            sort: true,
            classes: 'columnTable',
            formatter: (cell, row, rowIndex) => {
                return (<b key={rowIndex}>{row.proximidadeSemantica}%</b>)
            }
        },
        {
            dataField: 'filename',
            text: '',
            formatter: (cell, row, rowIndex) => {
                if (row.filename) {
                    return (<Button key={rowIndex} title="Baixar documentos" variant="outline-primary" onClick={() => downloadDocuments(row.filename, rowIndex)}
                    ><FontAwesomeIcon icon={faDownload} size="lg">
                        </FontAwesomeIcon></Button>)
                }
                else {
                    return <div><p index={rowIndex}></p></div>
                }
            }, headerStyle: (colum, colIndex) => {
                return { width: '3vw', textAlign: 'center' };
            }

        }

    ];
    columns = [
        {
            dataField: 'representacaoId',
            text: "Representacao ID",
            hidden: true,
        }, {
            dataField: 'representacaoProcesso',
            text: 'Número do processo',
            sort: true,
            classes: 'columnTable',
            headerFormatter: this.formatHeader,
        }, {
            dataField: 'representacaoTitulo',
            text: 'Título',
            sort: true,
            classes: 'columnTable',
            headerFormatter: this.formatHeader,
        }, {
            dataField: 'representacaoDescricao',
            text: 'Descrição',
            sort: true,
            classes: 'columnTable',
            headerFormatter: this.formatHeader,
        },
        {
            dataField: 'decisaoId',
            text: '',
            formatExtraData: this.props,
            formatter: (cell, row, rowIndex, formatExtraData) => {
                return (
                    <Button title="Representações próximas" variant="outline-primary" onClick={() => formatExtraData.getEditaisProximos(row, rowIndex, this.props.clickedIndex)}
                    ><FontAwesomeIcon icon={faColumns} size="lg">
                        </FontAwesomeIcon></Button>
                )
            }, headerStyle: (colum, colIndex) => {
                return { width: '3vw', textAlign: 'center' };
            }
        },

        {
            dataField: 'decisaoProcesso',
            text: '',
            formatter: (cell, row, rowIndex) => {
                if (row.representacaoArquivos) {
                    return (<Button title="Baixar documentos" variant="outline-primary" onClick={() => downloadDocuments(row.representacaoArquivos)}
                    ><FontAwesomeIcon icon={faDownload} size="lg">
                        </FontAwesomeIcon></Button>)
                }
                else {
                    return null
                }
            }, headerStyle: (colum, colIndex) => {
                return { width: '3vw', textAlign: 'center' };
            }

        }];

    rowStyle = (row, rowIndex) => {
        const clickedIndex = this.props.clickedIndex
        console.log(clickedIndex)
        const style = {};
        if (clickedIndex !== null && rowIndex === clickedIndex) {
            style.backgroundColor = 'rgba(80, 116, 138,0.1)';
        }
        return style;

    }
    expandRowProximos = {
        renderer: row => (
            <div className="divDecisaoProx">
                {(row.filhoTitulo && row.filhoProcesso) && row.filhoTitulo !== "" && row.filhoProcesso !== "" ?
                    <div>
                        <p className="textDecisao"><b>Decisão</b></p>
                        <hr />
                        <p className="textDecisaoInt"><b>Título:</b> {row.filhoTitulo}</p>
                        <p className="textDecisaoInt"><b>Número processo:</b> {row.filhoProcesso}</p>
                        <p className="textDecisaoInt"><b>Descrição:</b> {row.filhoDescricao}</p>
                        <div className="internaDecisao">
                            <Button title="Editar texto da decisão" variant="outline-primary" onClick={() => this.openTextDecisao(row)}
                            ><FontAwesomeIcon icon={faEdit} size="lg">
                                </FontAwesomeIcon></Button>
                            <Button title="Baixar Decisão" variant="outline-primary" onClick={() => downloadDocuments(row.filhoFilename)}
                            ><FontAwesomeIcon icon={faDownload} size="lg">
                                </FontAwesomeIcon></Button>
                            <Button title="Baixar Decisão em DOCX" variant="outline-primary" onClick={() => this.downloadDocx(row)}
                            ><FontAwesomeIcon icon={faFileDownload} size="lg">
                                </FontAwesomeIcon></Button></div>

                    </div>
                    :
                    <h6 className="textEmpyt">Não há decisão para essa representação.</h6>
                }
            </div>
        ),
        expandHeaderColumnRenderer: ({ isAnyExpands }) => {
            if (isAnyExpands) {
                return null;
            }
            return null;
        },
        expandColumnRenderer: ({ expanded }) => {
            if (expanded) {
                return (
                    <div className="indicador"><FontAwesomeIcon icon={faAngleUp} size="lg">
                    </FontAwesomeIcon></div>
                );
            }
            return (
                <div className="indicador"><FontAwesomeIcon icon={faAngleDown} size="lg">
                </FontAwesomeIcon></div>
            );
        },
        onlyOneExpanding: true,
        expandByColumnOnly: true,
        showExpandColumn: true,
    };
    options = {

        hideSizePerPage: true,
        paginationSize: 4,
        sizePerPage: 9,
        //paginationPosition: 'top'
        // alwaysShowAllBtns: true // Always show next and previous button
        // withFirstAndLast: false > Hide the going to First and Last page button

    }
    expandRow = {
        renderer: row => (
            <div className="mainDivInterna">
                {this.renderPalavrasChaves(row.representacaoPalavrasChaves)}
                {row.decisaoProcesso !== '' ?
                    <Row className="RowDecisaoExpand">
                        <div className="divInternaRepresentacao">
                            <p className="textDecisao"><b>Decisão</b></p>
                            <hr />
                            <p className="textDecisaoInt"><b>Título:</b> {row.decisaoTitulo}</p>
                            <p className="textDecisaoInt"><b>Número processo:</b> {row.decisaoProcesso}</p>
                            <p className="textDecisaoInt"><b>Descrição:</b> {row.decisaoDescricao}</p>
                            <Button className="btnDownload" title="Baixar documentos" variant="outline-primary" onClick={() => downloadDocuments(row.decisaoArquivos)}
                            ><FontAwesomeIcon icon={faDownload} size="lg">
                                </FontAwesomeIcon></Button>
                        </div>
                    </Row>
                    : null}
            </div>

        ),
        expandHeaderColumnRenderer: ({ isAnyExpands }) => {
            if (isAnyExpands) {
                return null;
            }
            return null;
        },
        expandColumnRenderer: ({ expanded }) => {
            if (expanded) {
                return (
                    <div className="indicador"><FontAwesomeIcon icon={faAngleUp} size="lg">
                    </FontAwesomeIcon></div>
                );
            }
            return (
                <div className="indicador"><FontAwesomeIcon icon={faAngleDown} size="lg">
                </FontAwesomeIcon></div>
            );
        },
        onExpand: (row, isExpand, rowIndex, e) => {
            console.log(row)
            this.props.getWordClound(row.representacaoId)
        },
        onlyOneExpanding: true,
        expandByColumnOnly: true,
        showExpandColumn: true,
    };
    options = {

        hideSizePerPage: true,
        paginationSize: 4,
        sizePerPage: 9,
        //paginationPosition: 'top'
        // alwaysShowAllBtns: true // Always show next and previous button
        // withFirstAndLast: false > Hide the going to First and Last page button
    };
    render() {
        return (
            <div className="divTableRepresentacao">
                {console.log(this.props)}
                <div className="tableConsultar">
                    {console.log(this.props.listaRepresentacoes)}
                    {this.props.listaRepresentacoes !== null && this.props.listaRepresentacoes.length > 0 ?
                        <BootstrapTable
                            bootstrap4
                            keyField='representacaoId'
                            data={this.props.listaRepresentacoes ? this.props.listaRepresentacoes : null}
                            columns={this.columns}
                            expandRow={this.expandRow}
                            pagination={paginationFactory(this.options)}
                            rowStyle={this.rowStyle}
                        />
                        : console.log("erro")}

                </div>
                <div>
                    <Modal aria-labelledby="contained-modal-title-vcenter" dialogClassName="modal-90w"
                        centered show={this.props.handleShow} >
                        <Modal.Header >
                            <Button title="Sair." className="BtnLogoutModal" onClick={() => this.props.setHendler(false)} variant="primary" >
                                <FontAwesomeIcon className="iconCloseModal" icon={faTimes}
                                    size="lg">
                                </FontAwesomeIcon>
                            </Button>
                        </Modal.Header>
                        <Modal.Body>

                            <div className="modalProximos">
                                <div className="tituloHeaderProximos">
                                    <p className="tituloCardProximidade">Representações mais próximas com palavras chaves</p></div>
                                <div className="cardProximos">
                                    {this.props.listaEditaisProximosPC && this.props.listaEditaisProximosPC.length > 0 ?
                                        <BootstrapTable
                                            bootstrap4
                                            keyField="id"
                                            data={this.props.listaEditaisProximosPC ? this.props.listaEditaisProximosPC.slice(0, 5) : null}
                                            columns={this.columnsProximos}
                                            expandRow={this.expandRowProximos}
                                        />
                                        :
                                        <h6 className="textEmpyt">Não há representação próxima ou calculada com palavras chaves .</h6>
                                    }
                                </div>
                                <div className="tituloHeaderProximos">
                                    <p className="tituloCardProximidade">Representações mais próximas sem palavras chaves</p></div>
                                {console.log(this.props.listaEditaisProximos)}
                                <div className="cardProximos">
                                    {this.props.listaEditaisProximos && this.props.listaEditaisProximos.length > 0 ?
                                        <BootstrapTable
                                            bootstrap4
                                            keyField="id"
                                            data={this.props.listaEditaisProximos ? this.props.listaEditaisProximos.slice(0, 5) : null}
                                            columns={this.columnsProximos}
                                            expandRow={this.expandRowProximos}
                                        />
                                        : <div className="empytTable">
                                            <h6 className="textEmpyt">Não há representação próxima ou calculada sem palavras chaves .</h6>
                                        </div>}
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={() => this.props.setHendler(false)}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Modal>
                </div>

            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        listaRepresentacoes: state.listaRepresentacoes.listaRepresentacoes,
        listaEditaisProximos: state.listaEditaisProximos.listaEditaisProximos,
        clickedIndex: state.clickedIndex.clickedIndex,
        wordCloud: state.wordCloud.wordCloud,
        listaEditaisProximosPC: state.listaEditaisProximosPC.listaEditaisProximosPC,
        handleShow: state.handleShow.handleShow,
        Decisao_Texto: state.Decisao_Texto.Decisao_Texto

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ redirect, getEditaisProximos, clickIndex, getWordClound, setErrorData, openTextDecisao, setHendler, getTextDecisao, setHeader }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(RepresentacaoTable)