import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { setTela, changeRepresentacaoByEditalId, backToLista, setErrorData,setHeader } from './consultarActions'
import { Row, Col } from 'react-bootstrap';
import RepresentacaoTable from './representacaoTable'
import btBack from '../img/bt-voltar-01b.png'
import 'filepond/dist/filepond.min.css';
import './Style.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { faCaretLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";



class ConsultarRepresentacao extends Component {
    componentDidMount() {
        if (!this.props.listaRepresentacoes || this.props.listaRepresentacoes.length === 0) {
            this.props.history.push("/lista")
        }
        this.props.setHeader("rowHeader")
    }
    getMessenger() {
        if (this.props.consultarError && this.props.consultarError !== '') {
            NotificationManager.info(this.props.consultarError, 'Ops :|', 1200)
            this.props.setErrorData('')
        }
        if (this.props.consultarSucess && this.props.consultarSucess !== '') {
            console.log("entrou sucess edital")
            NotificationManager.success(this.props.consultarSucess, 'Successo :)', 1200)
            this.props.setSucessData('')
        }
    }
    render() {
        return (
            <div>
                {this.getMessenger()}
                <NotificationContainer />
                <div className="DivConsultar">
                <Row>
                    <Col className="ColleftBack" md={4} sm={12} xs={12}>
                        <button onClick={() => this.props.backToLista("/lista")} class="btn-font btn-font-back  btn-font-sep-back fa-chevron-left  ">Voltar</button>
                    </Col>
                    <Col className="ColleftBack" md={4} sm={12} xs={12}> </Col>
                    <Col className="ColleftBack" md={4} sm={12} xs={12}> </Col>

                </Row>
                    <div className="divInternaConsultar">
                        <Row className="RowElements">

                            <Col xs={12}>
                                <h4 className='titleEdital'>Representações do edital: {this.props.editalSelecionado.titulo}</h4>
                                <p className='textEdital'>Número do processo: {this.props.editalSelecionado.nrProcesso}
                                </p>

                            </Col>

                        </Row>
                        <RepresentacaoTable />
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        editalSelecionado: state.editalSelecionado.editalSelecionado,
        listaRepresentacoes: state.listaRepresentacoes.listaRepresentacoes,
        consultarSucess: state.consultarSucess.consultarSucess,
        consultarError: state.consultarError.consultarError
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ setTela, changeRepresentacaoByEditalId, backToLista, setErrorData,setHeader }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ConsultarRepresentacao)

