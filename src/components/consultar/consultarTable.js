import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Button } from 'react-bootstrap';
import { faDownload, faAddressCard, faEye,faTrashAlt} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { redirect, editEdital,setErrorData,setHeader,deleteEditallogico } from "../consultar/consultarActions"
import './Style.css';
import { getUrl } from '../../Constantes'
import axios from 'axios';






const downloadDocuments = (row) => {
    ;
    var src = row.documentosArquivos
    src = src.replace('}', '')
    src = src.replace('{', '')
    let srcs = src.split(',')
    srcs.forEach(element => {
        console.log(element)
        let url = getUrl(window.location.hostname).url_file_server + "download?object=" + element;
        axios({
            url: url,
            responseType: 'blob',
            method: 'GET',
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', element);
            document.body.appendChild(link);
            link.click();
        });
    })
}

class ConsultarTable extends Component {
    componentDidMount() {
        this.props.setHeader("rowHeader")
      }

    editFormater(cell, row) {
        if (row.documentosArquivos != null) {
            console.log()
            return (<div><Button title="Baixar documentos" className="iconsEditais" variant="outline-primary" onClick={() => downloadDocuments(row)}
            ><FontAwesomeIcon icon={faDownload} size="lg">
                </FontAwesomeIcon></Button></div>)
        }
    }
    editFormaterEditar(cell, row, props) {
        return (<div>
            <Button title="Ver edital" className="iconsEditais" variant="outline-primary" onClick={() => props.editEdital(row.documentoId)}
            ><FontAwesomeIcon icon={faEye} size="lg">
                </FontAwesomeIcon></Button></div>)
    }
    editFormaterRepresentacoes(cell, row, props) {
        return (<div>
            <Button title="Consultar representação" className="iconsEditais" variant="outline-primary" onClick={() => props.redirect("consultarRepresentacao", row)}
            ><FontAwesomeIcon icon={faAddressCard} size="lg">
                </FontAwesomeIcon></Button></div>)
    }
    editFormaterDeleteEdital(cell, row, props) {
        console.log(props)
        return (<div>
            <Button title="Consultar representação" className="iconsEditais" variant="outline-primary" onClick={() => props.deleteEditallogico(row,props.consultaValue)}
            ><FontAwesomeIcon icon={faTrashAlt} size="lg">
                </FontAwesomeIcon></Button></div>)
    }
    showMensage(){
        if(this.props.tipo && this.props.tipo.length === 0){
            this.props.setErrorData("Não há dados para sua pesquisa.")
        }
    }
    render() {
        const options = {

            hideSizePerPage: true,
            paginationSize: 4,
            sizePerPage: 9,
            //paginationPosition: 'top'
            // alwaysShowAllBtns: true // Always show next and previous button
            // withFirstAndLast: false > Hide the going to First and Last page button
        }
        return (
            <div>
                {this.props.tipo && this.props.tipo.length > 0 ?
                    <BootstrapTable
                        data={this.props.tipo}
                        pagination className="tableConsultar" trClassName="tr" options={options}>   
                        <TableHeaderColumn dataSort={true} dataAlign='left' dataField='nrProcesso' isKey={true}>Número do processo</TableHeaderColumn>
                        <TableHeaderColumn dataSort={true} dataAlign='left' dataField='titulo'>Título</TableHeaderColumn>
                        <TableHeaderColumn dataSort={true} dataAlign='left' dataField='documentosDescricao'>Descrição</TableHeaderColumn>
                        <TableHeaderColumn width={'4%'} dataAlign='right' dataField="" dataFormat={this.editFormaterEditar} formatExtraData={this.props}> </TableHeaderColumn>
                        <TableHeaderColumn width={'4%'} dataAlign='right' dataField="" dataFormat={this.editFormaterRepresentacoes} formatExtraData={this.props}></TableHeaderColumn>
                        <TableHeaderColumn width={'4%'} dataAlign='right' dataField="" dataFormat={this.editFormater} formatExtraData={this.props}> </TableHeaderColumn>
                        <TableHeaderColumn width={'4%'} dataAlign='right' dataField="" dataFormat={this.editFormaterDeleteEdital} formatExtraData={this.props}> </TableHeaderColumn>
                    </BootstrapTable>
                    :this.showMensage()}
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        tipo: state.tipo.tipo,
        consultaValue: state.consultaValue.consultaValue

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ redirect, editEdital,setErrorData,setHeader,deleteEditallogico }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ConsultarTable)