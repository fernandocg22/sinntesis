import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getEditais, changeTipo, searchDocuments, changeDocument, setTela, setErrorData, setHeader } from './consultarActions'
import { Row, Col, Form, Button, InputGroup } from 'react-bootstrap';
import { faSearch, faCaretLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ConsultarTable from './consultarTable'
import btBack from '../img/bt-voltar-01b.png'
import Loading from '../loading/loading'
import 'filepond/dist/filepond.min.css';
import './Style.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import Header from "../header/header"



class Consultar extends Component {
    componentDidMount() {
        this.props.setHeader("rowHeader")
    }
    search(event, inputPesquisa) {
        console.log(event)
        if (event.key === "Enter") {
            if (inputPesquisa !== "") {
                this.props.getEditais(inputPesquisa)
            }
        }
    }
    getMessenger() {
        if (this.props.consultarError && this.props.consultarError !== '') {
            NotificationManager.info(this.props.consultarError, 'Ops :|', 1200)
            this.props.setErrorData('')
        }
        if (this.props.consultarSucess && this.props.consultarSucess !== '') {
            console.log("entrou sucess edital")
            NotificationManager.success(this.props.consultarSucess, 'Successo :)', 1200)
            this.props.setSucessData('')
        }
    }
    render() {
        return (
            <div>
                {this.props.loading === true ? <Loading /> : null}
                {this.getMessenger()}
                <NotificationContainer />
                <div className="DivConsultar">
                    <Row >
                        <Col className="ColleftBack" md={4} sm={12} xs={12}>
                            <button onClick={() => this.props.setTela("/panel")} class="btn-font btn-font-back  btn-font-sep-back fa-chevron-left  ">Voltar</button>
                        </Col>
                        <Col className="ColleftBack" md={4} sm={12} xs={12}> </Col>
                        <Col className="ColleftBack" md={4} sm={12} xs={12}> </Col>

                    </Row>
                    <div className="divInternaConsultar">
                        <Row className="RowElements">
                            <Col>
                                <Form.Label className='textLabel'>Pesquisa de editais:</Form.Label>
                                <InputGroup>
                                    <Form.Control onKeyPress={(event) => this.search(event, this.refs.inputPesquisa.value)} ref="inputPesquisa" className="inputTextConsultar" value={this.props.Edital_processo} type="text" placeholder="Digite um número de processo ou palavra chave" />
                                    <InputGroup.Append>
                                        <Button title="Buscar edital." className="BtnPesquisa" onClick={() => this.refs.inputPesquisa.value !== "" ? this.props.getEditais(this.refs.inputPesquisa.value) : this.props.setErrorData("Por favor insira um proceso ou palavra chave para a pesquisa.")} variant="primary" type="submit">
                                            <FontAwesomeIcon className="iconSerach" icon={faSearch}
                                                size="lg">
                                            </FontAwesomeIcon>
                                        </Button>
                                    </InputGroup.Append>
                                </InputGroup>
                            </Col>

                        </Row>
                        <ConsultarTable />
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        tipo: state.tipo.tipo,
        document: state.document.document,
        tipo_selecionado: state.tipo_selecionado.tipo_selecionado,
        proximidade: state.proximidade.proximidade,
        document_selecionado: state.document_selecionado.document_selecionado,
        files: state.files.files,
        consultarSucess: state.consultarSucess.consultarSucess,
        consultarError: state.consultarError.consultarError,
        loading: state.loading.loading
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getEditais, changeTipo, searchDocuments, changeDocument, setTela, setErrorData, setHeader }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Consultar)