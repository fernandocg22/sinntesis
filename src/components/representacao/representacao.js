import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col, Form, Button} from 'react-bootstrap';
import {
    changeTipo, getPessoaJuridica, changePessoaJuridica, changeProcesso,
    changeDescricao, addFileItem, addDocument, changePond, showAlert, TraingDocument, addInput,
    changePalavraChave, deleteInput, setTela, changeTitulo, getEditais, changeEdital, setSucessData, setErrorData,setWarningData,getLeis, addInputLeis, deleteInputLeis,setHeader
} from './representacaoActions'
import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import './Style.css';
import { getUrl } from '../../Constantes'
import { faTimes, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Loading from '../loading/loading'
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import { Typeahead } from 'react-bootstrap-typeahead';
import TextField from '@material-ui/core/TextField';
import 'react-bootstrap-typeahead/css/Typeahead.css';


class Representacao extends Component {
    UNSAFE_componentWillMount() {
        this.props.getEditais()
        this.props.getLeis()
        this.props.setHeader("rowHeader")
    }
    renderPalavrasChaves() {
        return (
            <div className="rowPalavasChaves">
                <section className="containerPalavraChave flex flex-wrap">

                    {this.props.Representacao_palavras_chaves.map((item, index) => (
                        <div key={index} className="box">
                            <div>
                                <p className="textoPalavraChave">{item.text}</p></div>
                            <div className="divBtn"><Button onClick={() => this.props.deleteInput(index)} className="btnClose" variant="outline-primary"><FontAwesomeIcon className="iconClose" icon={faTimes} /></Button></div>
                        </div>
                    ))}
                </section>
            </div>
        )
    }
    add(event) {
        if (event.key === "Enter") {
            this.props.addInput(this.props.Representacao_palavras_chaves, this.refs.RepresentacaoinputPalavraChave.value)
            this.refs.RepresentacaoinputPalavraChave.value = '';
        }
    }
    addPalavraChave() {
        this.props.addInput(this.props.Representacao_palavras_chaves, this.refs.RepresentacaoinputPalavraChave.value)
        this.refs.RepresentacaoinputPalavraChave.value = '';
    }
    getMessenger() {
        if (this.props.errorRepresentacao && this.props.errorRepresentacao !== '') {
            this.props.setErrorData('')
            NotificationManager.error(this.props.errorRepresentacao, 'Erro :(', 3000)

        }
        if (this.props.sucessRepresentacao && this.props.sucessRepresentacao !== '') {
            this.props.setSucessData('')
            NotificationManager.success(this.props.sucessRepresentacao, 'Successo :)', 3000)
        }
        if (this.props.warningRepresentacao && this.props.warningRepresentacao !== '') {
            this.props.setWarningData('')
            NotificationManager.info(this.props.warningRepresentacao, 'Dados preenchidos incorretamente. :|', 3000)
        }
    }
    render() {
        return (
            <div>
                {this.props.Representacao_loading === true ? <Loading /> : null}
                {this.getMessenger()}
                <NotificationContainer />
                <div className="DivrRepresentacao">
                <Row >
                    <Col className="ColleftBack" md={4} sm={12} xs={12}>
                        <button onClick={() => this.props.setTela("/paneledital", this.props)} class="btn-font btn-font-back  btn-font-sep-back fa-chevron-left  ">Voltar</button>
                    </Col>
                    <Col className="ColleftBack" md={8} sm={12} xs={12}> </Col>

                </Row>
                <Row>
                    <Col className="ColleftTitle" md={12} sm={12} xs={12}>
                            <h4 className='titleEdital'>INSERIR REPRESENTAÇÃO</h4>
                    </Col>
                </Row>
                    <div className="divInterna">
                        {console.log(this.props)}
                        <Form.Group controlId="formBasicEmail">
                            <Row className="RowElements">
                                <Col sm={12} md={3} lg={3}>
                                    <Form.Label className='textLabel'>Edital:</Form.Label>                                    
                                    <Typeahead
                                        labelKey={(option) => `${option.titulo}`}
                                        id="basic-example"
                                        onChange={(e) => this.props.changeEdital(e)}
                                        options={this.props.Representacao_edital ? this.props.Representacao_edital:null}
                                        placeholder="Selecione o edital"
                                        renderInput={params => <TextField {...params} label="debug" margin="normal" fullWidth />}
                                    />
                                </Col>
                                <Col sm={12} md={9} lg={9}>
                                    <Form.Label className='textLabel'>Título:</Form.Label>
                                    <Form.Control className="inputText" onChange={(e) => this.props.changeTitulo(e)} value={this.props.Representacao_titulo} type="email" placeholder="Digite o título" />
                                </Col>
                            </Row>
                            {console.log(this.props.Representacao_leis)}
                            {this.props.Representacao_leis_input.length && this.props.Representacao_leis_input.length > 0 ?
                                this.renderLeis()
                                : null}
                            <Row className="RowElements">
                                <Col className="rowPalavasChaves" xs={6}>
                                    <Form.Label className='textLabel'>Leis:</Form.Label>
                                    <div className="divRowPalavraChave">
                                        <Typeahead
                                            ref="RepresentacaoinputLeis"
                                            labelKey={(option) => `${option.leiNome}`}
                                            id="basic-example"
                                            options={this.props.Representacao_leis ? this.props.Representacao_leis : null}
                                            placeholder="Selecione o edital"
                                            renderInput={params => <TextField {...params} label="debug" margin="normal" fullWidth />}
                                        />
                                        <Button onClick={() => this.addLeiClick()} size="sm" className="btnPalavraChave" variant="outline-primary"
                                        ><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                                    </div>
                                </Col>
                            </Row>
                            <Row className="RowElements">
                                <Col xs={12}>
                                    <Form.Label className='textLabel'>Descrição:</Form.Label>
                                    <Form.Control as="textarea" className="textDescricao" onChange={(e) => this.props.changeDescricao(e)} value={this.props.Representacao_Descricao} placeholder="Digite a descrição" />
                                </Col>
                            </Row>
                            {this.props.Representacao_palavras_chaves.length > 0 ?
                                this.renderPalavrasChaves()
                                : null}
                            <Row>
                                <Col className="rowPalavasChaves" xs={4}>
                                    <Form.Label className='textLabel'>Palavra chave:</Form.Label>
                                    <div className="divRowPalavraChave">
                                        <Form.Control ref="RepresentacaoinputPalavraChave" onKeyPress={(event) => this.add(event, this.props.Representacao_palavras_chaves, this.refs.RepresentacaoinputPalavraChave.value)} className="inputText" type="email" placeholder={"Palavras chaves"} />
                                        <Button onClick={() => this.addPalavraChave()} size="sm" className="btnPalavraChave" variant="outline-primary"
                                        ><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                                    </div>
                                </Col>
                            </Row>
                            <Row className="filePond">
                                <Col>
                                    <Form.Label className='textLabel'>Upload de arquivos:</Form.Label>
                                    <FilePond
                                        allowMultiple={true}
                                        ref={ref => this.pond = ref}
                                        onupdatefiles={(fileItens) => {
                                            this.props.addFileItem(fileItens)
                                            this.props.changePond(this.pond)
                                        }}
                                        labelIdle='Arraste e solte seus arquivos ou navegue.'
                                        server={
                                            {
                                                url: getUrl(window.location.hostname).url_file_server,
                                                process: {
                                                    headers: {
                                                        'Authorization': 'Bearer' + ' ' + JSON.parse(window.sessionStorage.getItem('dataUsers')).token
                                                    },
                                                },
                                                load: {
                                                    headers: {
                                                        'Authorization': 'Bearer' + ' ' + JSON.parse(window.sessionStorage.getItem('dataUsers')).token
                                                    },
                                                }

                                            }


                                        } /></Col>

                            </Row>
                            <Row className="rowSubmit">
                                <div >
                                    <Button className="BtnSubmit" onClick={() => this.props.addDocument(this.props)} variant="primary"  >
                                        Salvar
                        </Button>
                                </div>

                            </Row>
                        </Form.Group>
                        <Row className="controlMargin">
                        </Row>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        Representacao_pessoa_juridica: state.Representacao_pessoa_juridica.Representacao_pessoa_juridica,
        Representacao_pessoa_juridica_selecionada: state.Representacao_pessoa_juridica_selecionada.Representacao_pessoa_juridica_selecionada,
        Representacao_processo: state.Representacao_processo.Representacao_processo,
        Representacao_Descricao: state.Representacao_Descricao.Representacao_Descricao,
        Representacao_files: state.Representacao_files.Representacao_files,
        Representacao_pond: state.Representacao_pond.Representacao_pond,
        Representacao_sucess: state.Representacao_sucess.Representacao_sucess,
        Representacao_palavras_chaves: state.Representacao_palavras_chaves.Representacao_palavras_chaves,
        Representacao_titulo: state.Representacao_titulo.Representacao_titulo,
        Representacao_edital_selecionado: state.Representacao_edital_selecionado.Representacao_edital_selecionado,
        Representacao_edital: state.Representacao_edital.Representacao_edital,
        Representacao_loading: state.Representacao_loading.Representacao_loading,
        sucessRepresentacao: state.sucessRepresentacao.sucessRepresentacao,
        errorRepresentacao: state.errorRepresentacao.errorRepresentacao,
        warningRepresentacao: state.warningRepresentacao.warningRepresentacao,
        Representacao_lei_selecionado: state.Representacao_lei_selecionado.Representacao_lei_selecionado,
        Representacao_leis: state.Representacao_leis.Representacao_leis,
        Representacao_leis_input: state.Representacao_leis_input.Representacao_leis_input
        


    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        changeTipo, getPessoaJuridica, changePessoaJuridica,
        changeProcesso, changeDescricao, addFileItem,
        addDocument, changePond, showAlert, TraingDocument,
        addInput, changePalavraChave, deleteInput, setTela, changeTitulo, getEditais, changeEdital, setSucessData, setErrorData,setWarningData,getLeis, addInputLeis, deleteInputLeis,setHeader
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Representacao)