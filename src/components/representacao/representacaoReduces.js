const INITIAL_STATE = {
    Representacao_tipo: [], Representacao_tipo_selecionado: [], Representacao_pessoa_juridica: [], Representacao_pessoa_juridica_selecionada: [], Representacao_processo: [], Representacao_Descricao: [], Representacao_files: [], Representacao_pond: null, Representacao_sucess: false,

    Representacao_palavras_chaves: [], Representacao_titulo: '', Representacao_edital: [], errorRepresentacao: '', sucessRepresentacao: '', warningRepresentacao: '', Representacao_leis: [], Representacao_leis_input: []
}
export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'GET_TIPO':
            return { ...state, Representacao_tipo: action.payload }
        case 'GET_TIPO_SELECIONADO':
            return { ...state, Representacao_tipo_selecionado: action.payload }
        case 'ADD_INPUT_REPRESENTACAO':
            return { ...state, Representacao_palavras_chaves: [...state.Representacao_palavras_chaves, action.payload] }
        case 'ADD_INPUT_LEIS':
            return { ...state, Representacao_leis_input: [...state.Representacao_leis_input, action.payload] }
        case 'SET_LOADING_REPRESENTACAO':
            return { ...state, Representacao_loading: action.payload }
        case 'SET_TITULO_REPRESENTACAO':
            return { ...state, Representacao_titulo: action.payload }
        case 'GET_PESSOA_JURIDICA_SELECIONADA':
            return { ...state, Representacao_pessoa_juridica_selecionada: action.payload }
        case 'GET_PROCESSO_SELECIONADO':
            return { ...state, Representacao_processo: action.payload }
        case 'GET_REPRESENTACAO_DESCRICAO_SELECIONADA':
            return { ...state, Representacao_Descricao: action.payload }
        case 'GET_FILE_NAMES':
            return { ...state, Representacao_files: action.payload }
        case 'GET_POND':
            return { ...state, Representacao_pond: action.payload }
        case 'GET_EDITAL_SELECIONADO':
            return { ...state, Representacao_edital_selecionado: action.payload }
        case 'GET_EDITAIS':
            return { ...state, Representacao_edital: action.payload }
        case 'GET_LEIS':
            return { ...state, Representacao_leis: action.payload }
        case 'GET_SUCESS_MENSAGER_REPRESENTACAO':
            return { ...state, sucessRepresentacao: action.payload }
        case 'GET_ERROR_MENSAGER_REPRESENTACAO':
            return { ...state, errorRepresentacao: action.payload }
        case 'GET_WARNING_MENSAGER_REPRESENTACAO':
            return { ...state, warningRepresentacao: action.payload }
        case 'REMOVE_INPUT_LEIS':
            return {
                ...state,
                Representacao_leis_input: state.Representacao_leis_input.filter((item, index) => index !== action.payload)
            }
        case 'REMOVE_INPUT':
            return {
                ...state,
                Representacao_palavras_chaves: state.Representacao_palavras_chaves.filter((item, index) => index !== action.payload)
            }
        case "RESET_REPRESENTACAO":
            return {
                ...state, Representacao_tipo: [],
                Representacao_tipo_selecionado: [],
                Representacao_pessoa_juridica: [],
                Representacao_pessoa_juridica_selecionada: [],
                Representacao_processo: [],
                Representacao_Descricao: [],
                Representacao_files: [],
                Representacao_pond: null,
                Representacao_sucess: true,
                Representacao_leis: [],
                Representacao_edital: [],
                Representacao_titulo: '',
                Representacao_palavras_chaves: [],
                Representacao_leis_input: [],
                errorRepresentacao: '',
                sucessRepresentacao: ''
            }
        case "SHOW_ALERT":
            return { ...state, Representacao_sucess: action.payload }
        default:
            return state
    }
}               