import axios from 'axios';
import moment from 'moment';
import { getUrl } from '../../Constantes'
import { push } from 'connected-react-router'
const base_url = getUrl(window.location.hostname).base_url
const base_url_training = getUrl(window.location.hostname).base_url_training


export const addTicketData = (data) => {
    return {
        type: 'GET_TIPO',
        payload: data
    }
};

export const setLoading = (data) => {
    return (dispatch) => {
        dispatch({
            type: 'SET_LOADING_REPRESENTACAO',
            payload: data
        })
    }
};

export const addDescricaoSelecionada = (data) => {
    return {
        type: 'GET_REPRESENTACAO_DESCRICAO_SELECIONADA',
        payload: data
    }
};

export const setWarningData = (data) => {
    return {
        type: 'GET_WARNING_MENSAGER_REPRESENTACAO',
        payload: data
    }
};


export const setPalavraChave = (data) => {
    return {
        type: 'SET_PALAVRA_CHAVE',
        payload: data
    }
}
export function changePalavraChave(index, texto) {
    let array = {
        index: index,
        texto: texto
    }
    return dispatch => {
        dispatch(setPalavraChave(array))
    }
}

export const addInputData = (data) => {
    return {
        type: 'ADD_INPUT_REPRESENTACAO',
        payload: data
    }
}

export function addInput(props, text) {
    let array = {
        id: props.length,
        text: text
    }
    return (dispatch) => {
        dispatch(addInputData(array))
    }
}

export const changeEditalData = (data) => {
    return {
        type: 'GET_EDITAL_SELECIONADO',
        payload: data
    }
}

export function changeEdital(e) {
    console.log(e)
    return (dispatch) => {
        dispatch(changeEditalData(e[0]))
    }
}

export const removeInputData = (data) => {
    return {
        type: 'REMOVE_INPUT',
        payload: data
    }
}

export function deleteInput(id) {
    console.log(id)
    return (dispatch) => {
        dispatch(removeInputData(id))
    }
}

export const addEditalData = (data) => {
    return {
        type: 'GET_EDITAIS',
        payload: data
    }
};

export function getEditais() {
    return dispatch => {
        axios.get(base_url + "documento/1").then(
            resp => {
                dispatch(addEditalData(resp.data)
                )
            }
        )

    }

}



export const addTipoSelecionado = (data) => {
    return {
        type: 'GET_TIPO_SELECIONADO',
        payload: data
    }
};

export function changeTipo(e) {
    return dispatch => {
        axios.get(base_url + "tipoDocumento/" + e).then(
            resp => {
                dispatch(addTipoSelecionado(resp.data))
            })
    }
}

export const addPessoaJuridicaSelecionada = (data) => {
    return {
        type: 'GET_PESSOA_JURIDICA_SELECIONADA',
        payload: data
    }
};

export function changePessoaJuridica(e) {
    return dispatch => {
        axios.get(base_url + "pessoaJuridica/" + e.target.value).then(
            resp => {
                dispatch(addPessoaJuridicaSelecionada(resp.data))
            })
    }
}

export const addProcessoSelecionado = (data) => {
    return {
        type: 'GET_PROCESSO_SELECIONADO',
        payload: data
    }
};

export function changeProcesso(e) {
    return dispatch => {
        dispatch(addProcessoSelecionado(e.target.value))
    }
}


export function changeDescricao(e) {
    return dispatch => {
        dispatch(addDescricaoSelecionada(e.target.value))
    }
}



export const addPessoaJuridicaData = (data) => {
    return {
        type: 'GET_PESSOA_JURIDICA',
        payload: data
    }
};

export function getPessoaJuridica() {
    return dispatch => {
        axios.get(base_url + "pessoaJuridica").then(
            resp => {
                dispatch(addPessoaJuridicaData(resp.data)
                )
            }
        )

    }

}
export const resetState = (data) => {
    console.log(data)
    if(data.Representacao_pond){
        data.Representacao_pond._pond.removeFiles()
    }
    return {
        type: 'RESET_REPRESENTACAO',
        payload: data
    }
};

export const setSucessData = (data) => {
    return {
        type: 'GET_SUCESS_MENSAGER_REPRESENTACAO',
        payload: data
    }
};

export const setErrorData = (data) => {
    return {
        type: 'GET_ERROR_MENSAGER_REPRESENTACAO',
        payload: data
    }
};

function postDocument(element,data) {
    return dispatch => {
        axios.post(base_url + 'documento', element).then(
            resp => {
                //post leitura.
                //dispatch(processaDocumento(data,resp.data))
                let mensager = "dados enviados com sucesso"
                dispatch(resetState(data))
                dispatch(setSucessData(mensager))
                dispatch(setLoading(false))
            }

        ).catch(function (error) {
            dispatch(setErrorData("Houve um erro ao gravar os dados."))
            dispatch(setLoading(false))
        });
    }
}


function processaDocumento(data,respdata) {
    console.log(data)
    return dispatch => {
        axios.get(base_url_training + 'leitura').then(
            resp => {

                if(resp.data.success === false){
                    dispatch(disableDocument(respdata,"Documento não atualizado devido a caracteres especiais."))
                }else{
                    dispatch(getEditais())
                    let mensager = "dados enviados com sucesso"
                    dispatch(setSucessData(mensager))
                    dispatch(setLoading(false))
                    dispatch(resetState(data))
            }
        }

        ).catch(function (error) {
            dispatch(disableDocument(respdata,"Houve um erro ao gravar os dados."))
        });
    }
}

export function disableDocument(data,errorMensager){
    data.documentoAtivo = "0"
    return dispatch => {
        axios.post(base_url + 'documento', data).then(
            resp => {
                dispatch(setErrorData(errorMensager))
                dispatch(setLoading(false))
            }

        ).catch(function (error) {
            dispatch(setErrorData(errorMensager))
            dispatch(setLoading(false))
        });
    }
}

export const showAlert_data = (data) => {
    return {
        type: 'SHOW_ALERT',
        payload: data
    }
};

export function showAlert(data) {
    return dispatch => {
        dispatch(showAlert_data(data))
    }
}

export const addTituloSelecionado = (data) => {
    return {
        type: 'SET_TITULO_REPRESENTACAO',
        payload: data
    }
}

export function changeTitulo(e) {
    return dispatch => {
        dispatch(addTituloSelecionado(e.target.value))
    }
}


export function addDocument(data) {

    return dispatch => {
        let palavrasChaves = ''
        let filename = []
        let leis = []
        data.Representacao_palavras_chaves.forEach(element => {
            if (palavrasChaves === '') {
                palavrasChaves = element.text
            }
            else {
                palavrasChaves = palavrasChaves + "," + element.text
            }
        })
        data.Representacao_leis_input.forEach(element => {
            leis.push(element.lei)
        })


        data.Representacao_files.forEach(element => {
            filename.push(element.filename)
        });
        filename = JSON.stringify(filename)
        filename = filename.replace(']', '')
        filename = filename.replace('[', '')
        var RegExp = /["|']/g;
        filename=filename.replace(RegExp,"");
        const deteTime = moment().format("DD/MM/YYYY hh:mm:ss")

        if(!data.Representacao_edital_selecionado){
            dispatch(setWarningData("Por favor selecione um edital."))
        }
        else{
            const _Data = {
                pessoaJuridica: {
                    "pessoaJuridicaId": 1,
                    "tipoPessoaJuridica": {
                        "tipoPessoaJuridicaId": 1,
                        "tipoPessoaJuridicaDescricao": "PJ",
                        "tipoPessoaJuridicaAtivo": "1"
                    }
                },
                tipoDocumento: {
                    "tipoDocumentoId": 2,
                    "tipoDocumentoDescricao": "representação",
                    "tipoDocumentoAtivo": "1"
                },
                "corpo": '',
                documentosArquivos: filename,
                titulo: data.Representacao_titulo,
                documentosDescricao: data.Representacao_Descricao,
                documentosDataCadastro: deteTime,
                documentosPalavrasChaves: palavrasChaves,
                documentoLei: leis,
                documentoProcessadoLeitura: '0',
                documentoPai: data.Representacao_edital_selecionado,
                nrProcesso: data.Representacao_edital_selecionado.nrProcesso,
                documentoAtivo: "1"
            }
            console.log(_Data)
            if(_Data.documentosArquivos===""){
                dispatch(setWarningData("Por favor adicione pelo menos um arquivo."))
            }else if(_Data.titulo===""){
                dispatch(setWarningData("Por favor preencha o titulo."))
             }
             else{
                 dispatch(setLoading(true));
                dispatch(postDocument(_Data,data))
             }

            }   


    }

}

export function TraingDocument() {
    axios.get(base_url_training + 'processa_documentos').then(
        resp => {
            console.log(resp.data)
        }

    ).catch(function (error) {
        console.log(error)
    });
}


export const addFileData = (data) => {
    console.log(data)
    return {
        type: 'GET_FILE_NAMES',
        payload: data
    }
};

export function addFileItem(item) {
    return dispatch => {
        dispatch(addFileData(item))
    }
}

export const addPondData = (data) => {
    return {
        type: 'GET_POND',
        payload: data
    }
}

export function changePond(pond) {
    console.log(pond)
    return dispatch => {
        dispatch(addPondData(pond))
    }
}
export const setTela = (url,data) => {
    return dispatch => {
        dispatch(push(url));
        dispatch(resetState(data));
    };
}
export const addInputLeisData = (data) => {
    return {
        type: 'ADD_INPUT_LEIS',
        payload: data
    }
}

export function addInputLeis(props, lei) {
    let array = {
        id: props.length,
        lei: lei
    }
    return (dispatch) => {
        dispatch(addInputLeisData(array))
    }
}
export const removeInputDataLeis = (data) => {
    return {
        type: 'REMOVE_INPUT_LEIS',
        payload: data
    }
}
export function deleteInputLeis(id) {
    console.log(id)
    return (dispatch) => {
        dispatch(removeInputDataLeis(id))
    }
}

export const addLeiData = (data) => {
    console.log(data)
    return {
        type: 'GET_LEIS',
        payload: data
    }
};

export function getLeis() {
    return dispatch => {
        axios.get(base_url + "lei").then(
            resp => {
                dispatch(addLeiData(resp.data)
                )
            }
        )

    }

}
export const setHeader = (data) => {
    return {
        type: 'SET_CLASS_HEADER_BACKGROUND',
        payload: data
    }
  }