import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col, Image, Button } from 'react-bootstrap';
import './Style.css';
import { setTela, changeNameLogin,setHeader } from './panelActions'


import { faFileSignature,faGavel } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


class PanelInicial extends Component {
    componentDidMount() {
        this.props.setHeader("rowHeader")
      }
    render() {
        return (
            <div className="divInternaBackground">
            <div className="paddingPanelInicial">
                {console.log(this.props)}
                <Row className="rowBtbBodyPanelInicial">
                    <Col className="ColCenter" xs={12} sm={12} md={4} >
                        <button onClick={() => this.props.setTela("/panel")} className="btn-font btn-font-1 btn-font-sep fa-file-text-o">Edital</button>
                    </Col>

                    <Col  className="ColCenter" xs={12} sm={12} md={4} >
                         <button onClick={() => this.props.setTela("/panelInicial")} className="btn-font btn-font-1 btn-font-sep fa-gavel">Processo</button>
                    </Col>

                </Row>
            </div>
            <div className="controlMarginFooterPanelInicial"></div>
            </div>
        )

    }
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ setTela, changeNameLogin,setHeader}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(PanelInicial)