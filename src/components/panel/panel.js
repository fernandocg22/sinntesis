import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col, Image, Button } from 'react-bootstrap';
import './Style.css';
import { setTela, changeNameLogin,setHeader } from './panelActions'


import { faFileMedical, faSearch, faCaretLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";



class Panel extends Component {
    componentDidMount() {
        this.props.setHeader("rowHeader")
      }
    render() {
        return (
            <div>
                <div className="divInternaBackground">
                <Row >
                    <Col className="ColleftBack" md={4} sm={12} xs={12}>
                        <button onClick={() => this.props.setTela("/panelInicial")} class="btn-font btn-font-back  btn-font-sep-back fa-chevron-left  ">Voltar</button>
                    </Col>
                    <Col className="ColleftBack" md={4} sm={12} xs={12}> </Col>
                    <Col className="ColleftBack" md={4} sm={12} xs={12}> </Col>

                </Row>

                <Row className="rowBtbBody">

                    <Col className="ColCenter" xs={12} sm={12} md={4} >
                        <button onClick={() => this.props.setTela("/paneledital")} className="btn-font btn-font-1 btn-font-sep fa-folder-open">Inserir  Documentos</button>
                    </Col>

                    <Col className="ColCenter" xs={12} sm={12} md={4} >
                        <button onClick={() => this.props.setTela("/lista")} className="btn-font btn-font-1 btn-font-sep fa-search">Consultar</button>
                    </Col>
                </Row>
                <Row className="rowBtbBody">

                    <Col className="ColCenter" xs={12} sm={12} md={4} >
                        <button onClick={() => this.props.setTela("/termos")} className="btn-font btn-font-1 btn-font-sep fa-clipboard">Termos</button>
                    </Col>

                    <Col className="ColCenter" xs={12} sm={12} md={4} >
                        <button onClick={() => this.props.setTela("/Leis")} className="btn-font btn-font-1 btn-font-sep fa-gavel">Leis</button>
                    </Col>
                </Row>
                <div className="controlMarginFooter"></div>
                </div>
            </div>
        )

    }
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ setTela, changeNameLogin,setHeader }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Panel)