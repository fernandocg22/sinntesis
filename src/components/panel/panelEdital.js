import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col,Button} from 'react-bootstrap';
import './Style.css';
import { setTela,setHeader } from './panelActions'
import { faSearch,faFileInvoice,faCaretLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";



class panelEdital extends Component {
    componentDidMount() {
        this.props.setHeader("rowHeader")
      }
    render() {
        return (
            <div>
                 <div className="divInternaBackground">
                <Row >
                    <Col className="ColleftBack" md={4} sm={12} xs={12}>  
                        <button  onClick={() => this.props.setTela("/panel")}  class="btn-font btn-font-back  btn-font-sep-back fa-chevron-left  ">Voltar</button>
                    </Col>
                    <Col className="ColleftBack" md={4} sm={12} xs={12}> </Col>
                    <Col className="ColleftBack" md={4} sm={12} xs={12}> </Col>
                    
                   </Row>
                    <Row className="rowBtbBodypanelEdital">
                    <Col className="Colleft" md={4} sm={12} xs={12}>                       
                         <button  onClick={() => this.props.setTela("/lista")}  class="btn-font btn-font-2  btn-font-sep2 fa-search ">Consultar Documentos</button>
                        </Col>
                    </Row>
                    <Row className="rowOptions">
                        <Col className="ColCenter" md={4} sm={12} xs={12}>
                        <button  onClick={() => this.props.setTela("/edital")} class="btn-font btn-font-1 btn-font-sep fa-file-text-o">Inserir Edital</button>
                        </Col>
                        <Col className="ColCenter" md={4} sm={12} xs={12}>
                        <button  onClick={() => this.props.setTela("/representacao")}  class="btn-font btn-font-1 btn-font-sep  fa-users">Inserir Representação</button>
                        </Col>
                        <Col className="ColCenter" md={4} sm={12} xs={12}>
                        <button onClick={() => this.props.setTela("/decisao")}  class="btn-font btn-font-1 btn-font-sep fa-check">Inserir Decisão</button>
                        </Col>
                    </Row>
                    <div className="controlMarginFooterEdital"></div>
                    </div>
            </div>
        )

    }
}

function mapStateToProps(state) {
    return {

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setTela,setHeader}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(panelEdital)