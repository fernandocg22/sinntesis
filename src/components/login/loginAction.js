import axios from 'axios';
import { getUrl } from '../../Constantes'
import { push} from 'connected-react-router'
const base_url = getUrl(window.location.hostname).base_url


export const setErrorData = (data) => {
    return {
        type: 'GET_ERROR_LOGIN',
        payload: data
    }
};

export function login(login, password) {
    const loginData = { "usuarioEmail": login, "usuarioSenha": password }
    return dispatch => {
        axios.post(base_url + 'login', loginData).then(
            resp => {
                dispatch(gatNameUser(login, resp.data))
            }

        ).catch(function (error) {
            dispatch(setErrorData("Usuário ou senha incorretos, por favor verifique os dados."))
        });
    }
}
export const changeLoginData = (data) => {
    return {
        type: 'CHANGE_USER_NAME',
        payload: data
    }
}

export function gatNameUser(data, dataToken) {
    console.log(dataToken)
    return dispatch => {
        axios.post(base_url + 'usuario/email/' ,
            data, { headers: { 'Content-Type': 'text/plain' } }).then(
                resp => {
                    const dataStorage = {"token": dataToken, username: resp.data.usuarioNome }
                    dispatch(push("/panelInicial"))
                    dispatch(changeLoginData(resp.data.usuarioNome))
                    dispatch(window.sessionStorage.setItem("dataUsers", JSON.stringify(dataStorage)));
                }

            ).catch(function (error) {

            });
    }
}

export function createUser(data) {
    return dispatch => {
        axios.post(base_url + 'usuario', data).then(
            resp => {
                dispatch(push("/"))
                dispatch(setSucessData("Usuário criado com sucesso."))
            }

        ).catch(function (error) {
            console.log(error)
        });
    }
}


export function redirect(path) {
    return dispatch => { dispatch(push(path)) }
}

export const setSucessData = (data) => {
    return {
        type: 'GET_SUCESS_LOGIN',
        payload: data
    }
};

export const setHeader = (data) => {
    return {
        type: 'SET_CLASS_HEADER_BACKGROUND',
        payload: data
    }
}
export function resetAllState(data){
    return {
        type: 'RESET_CONSULTAR',
        pyload: data
    }
}
