import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col, Form, Button } from 'react-bootstrap';
import {createUser,setErrorData,setHeader} from './loginAction'
import btBack from '../img/bt-voltar-01b.png'
import './Style.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import { faCaretLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class FormLogin extends Component {

    componentDidMount() {
        this.props.setHeader("rowHeader")}
    createUser(){
        const usuarioAtivo = "1";
        const usuarioNome = this.refs.inputNome.value
        const usuarioSobrenome =  this.refs.inputSobrenome.value
        const usuarioEmail = this.refs.inputEmail.value
        const usuarioSenha = this.refs.inputSenha.value
        const ConfirmacaoSenha = this.refs.inputConfirmacaoSenha.value

        const emailRegex = /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if(usuarioNome && usuarioSobrenome && usuarioSenha === ConfirmacaoSenha && emailRegex.test(usuarioEmail)){
            const data = {"usuarioNome":usuarioNome,"usuarioSobrenome":usuarioSobrenome,"usuarioEmail":usuarioEmail.toLowerCase(),"usuarioSenha":usuarioSenha,"usuarioAtivo":usuarioAtivo}
             this.props.createUser(data)
        }
        else if(usuarioSenha!==ConfirmacaoSenha){
            NotificationManager.info("A confirmação de senha não conrresponde com a digitada.",'Dados digitados incorreto ! :|',3000)
        }
        else if(!emailRegex.test(usuarioEmail)){
            NotificationManager.info("Por favor digite um e-mail válido.",'Dados digitados incorreto! :|',3000)
        }

    }

    getMessenger() {
        console.log(this.props.errorLogin)
        console.log(this.props.sucessLogin)
        if (this.props.errorLogin && this.props.errorLogin !== '') {
            NotificationManager.info(this.props.errorLogin, 'Login incorreto :|', 3000)
            this.props.setErrorData('')
        }
        if (this.props.sucessLogin && this.props.sucessLogin !== '') {
            NotificationManager.success(this.props.sucessLogin, 'Successo :)', 1200)
            this.props.setSucessData('')
        }
    }
    render() {
        return (
            <div>
                    {this.getMessenger()}
                    <NotificationContainer />
                    <Row >
                    <Col className="ColleftBack" md={4} sm={12} xs={12}>
                        <button onClick={() => this.props.history.push("/login")} class="btn-font btn-font-back  btn-font-sep-back fa-chevron-left  ">Voltar</button>
                    </Col>
                    <Col className="ColleftTitle" md={4} sm={12} xs={12}>
                        <div className="divTitleEdital">
                                    <h4 className='titleEdital'>CRIAR USUÁRIO</h4>
                            </div>

                    </Col>
                    <Col className="ColleftBack" md={4} sm={12} xs={12}> </Col>

                </Row>
                <div className="DivFormLogin">
                    <div className="divInternaFormLogin">
                        <Form.Group controlId="formBasicEmail">
                            <Row className="RowElements">
                            </Row>
                            <Row className="RowElements">
                                <Col sm={12} md={6} lg={6}>
                                    <Form.Label className='textLabel'>Nome:</Form.Label>
                                    <Form.Control className="inputText" ref="inputNome" type="text" placeholder="Digite o seu nome" />
                                </Col>
                                <Col  sm={12} md={6} lg={6}>
                                    <Form.Label className='textLabel'>Sobrenome:</Form.Label>
                                    <Form.Control ref="inputSobrenome" className="inputText" type="text" placeholder="Digite o seu sobrenome" />
                                </Col>
                            </Row>
                            <Row className="RowElements">
                                <Col  sm={12} md={6} lg={6}>
                                    <Form.Label className='textLabel'>Email:</Form.Label>
                                    <Form.Control ref="inputEmail" className="inputText"  type="email" placeholder="Digite o seu Email" />
                                </Col>
                            </Row>
                            <Row className="RowElements">
                            <Col  sm={12} md={6} lg={6}>
                                    <Form.Label className='textLabel'>Senha:</Form.Label>
                                    <Form.Control className="inputText" ref="inputSenha" type="password" placeholder="Digite a sua senha" />
                                </Col>
                                <Col  sm={12} md={6} lg={6}>
                                    <Form.Label className='textLabel'>Confirmação de senha:</Form.Label>
                                    <Form.Control className="inputText" type="password"  ref="inputConfirmacaoSenha"placeholder="Confirme a sua senha" />
                                </Col>
                            </Row>
                            <Row className="rowSubmitLogin">
                                <div >
                                    <Button className="BtnSubmit" onClick={() => this.createUser()}  variant="primary"  >
                                        Criar Usuário
                                    </Button>
                                </div>

                            </Row>


                        </Form.Group>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        errorLogin: state.errorLogin.errorLogin,
        sucessLogin: state.sucessLogin.sucessLogin


    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({createUser,setErrorData,setHeader}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(FormLogin)