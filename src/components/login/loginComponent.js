import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import { faUser,faUserTie } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {redirect,changeLoginData,resetAllState} from "./loginAction"
import {  Button } from 'react-bootstrap';
import './Style.css';

class LoginComponent extends Component {

    componentDidMount() {
        if(window.sessionStorage.getItem("dataUsers")){
            console.log(window.sessionStorage)
            console.log(this.props.LoginComponent)
            if(this.props.LoginComponent === null)
                this.props.changeLoginData(JSON.parse(window.sessionStorage.getItem('dataUsers')).username)   
        }
      }
    logout(){
        this.props.changeLoginData(null)
        this.props.resetAllState(null)
        window.sessionStorage.clear()
        console.log(this.props)
        this.props.redirect("/login")
    }
    render() {
        return (
            <div className="divLoginComponent">
                {console.log(this.props)}
                {this.props.LoginComponent ?
                    <div className="inlineComponent">
                        {console.log(this.props)}
                        <div>
                        <h6 className="textUserComponent">{this.props.LoginComponent}</h6>
                        <h6 onClick={()=>this.logout()} className="exitText">SAIR</h6>
                        </div>
                        <FontAwesomeIcon className="iconUser" icon={faUserTie}
                            size="lg">
                        </FontAwesomeIcon>
                    </div>
                    : null}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        LoginComponent: state.LoginComponent.LoginComponent
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({redirect,changeLoginData,resetAllState}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent)

