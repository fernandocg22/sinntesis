import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Form, Button, InputGroup, Row, Col } from 'react-bootstrap';
import imgPrincipal from '../img/QRcode.jpeg';
import { login, changeLoginData, redirect, setErrorData, setSucessData, setHeader } from './loginAction'
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { faAt, faAsterisk } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import 'react-notifications/lib/notifications.css';

import './Style.css';

class Login extends Component {
    loginEnter(event, login, password) {
        console.log(event)
        if (event.key === "Enter") {
            if ((login && login !== '') && (password && password !== '')) {
                this.props.login(login.toLowerCase(), password)
            }
        }
    }
    componentDidMount() {
        this.props.setHeader("rowHeader2")
        if (!window.sessionStorage.getItem("dataUsers")) {
            this.props.changeLoginData(null)
        }
    }
    login(login, password) {
        if ((login && login !== '') && (password && password !== '')) {
            this.props.login(login.toLowerCase(), password)
        }
    }

    getMessenger() {
        console.log(this.props.errorLogin)
        console.log(this.props.sucessLogin)
        if (this.props.errorLogin && this.props.errorLogin !== '') {
            NotificationManager.info(this.props.errorLogin, 'Login incorreto :|', 3000)
            this.props.setErrorData('')
        }
        if (this.props.sucessLogin && this.props.sucessLogin !== '') {
            NotificationManager.success(this.props.sucessLogin, 'Successo :)', 3000)
            this.props.setSucessData('')
        }
    }
    render() {
        return (
            <div>
                {this.getMessenger()}
                <NotificationContainer />
                <div>
                    {console.log(JSON.parse(window.sessionStorage.getItem('dataUsers')))}
                    <div>
                        <div className="divBody">
                            <Row className="RowElements">
                                <Col xs={12} sm={12} sm={6} lg={6}>
                                    <div className="left">
                                        {console.log(this.props)}
                                        <div>
                                            <h6 className="textTitleLogin">Seja bem vindo ao Sinntesis, efetue seu login para continuar.</h6>
                                        </div>
                                        <div>
                                            <InputGroup >
                                                <InputGroup.Prepend>
                                                    <Button title="Buscar edital." className="BtnInputLogin" onClick={() => this.refs.inputPesquisa.value !== "" ? this.props.getEditais(this.refs.inputPesquisa.value) : this.props.setErrorData("Por favor insira um proceso ou palavra chave para a pesquisa.")} variant="primary" type="submit">
                                                        <FontAwesomeIcon className="iconSerach" icon={faAt}
                                                            size="lg">
                                                        </FontAwesomeIcon>
                                                    </Button>
                                                </InputGroup.Prepend>
                                                <Form.Control size="lg" onKeyPress={(event) => this.loginEnter(event, this.refs.inputLogin.value, this.refs.password.value)} autoComplete="off" ref="inputLogin" className="inputLogin" value={this.props.Edital_processo} type="text" placeholder="Digite seu email" />
                                            </InputGroup>


                                            <InputGroup >
                                                <InputGroup.Prepend>
                                                    <Button title="Buscar edital." className="BtnInputLogin" onClick={() => this.refs.inputPesquisa.value !== "" ? this.props.getEditais(this.refs.inputPesquisa.value) : this.props.setErrorData("Por favor insira um proceso ou palavra chave para a pesquisa.")} variant="primary" type="submit">
                                                        <FontAwesomeIcon className="iconSerach" icon={faAsterisk}
                                                            size="lg">
                                                        </FontAwesomeIcon>
                                                    </Button>
                                                </InputGroup.Prepend>

                                                <Form.Control size="lg" onKeyPress={(event) => this.loginEnter(event, this.refs.inputLogin.value, this.refs.password.value)} autoComplete="off" ref="password" className="inputLogin" value={this.props.Edital_processo} type="password" placeholder="Digite seu senha" />
                                            </InputGroup>
                                        </div>
                                        <div className="rowBtnLogin">
                                            <Button onClick={() => this.login(this.refs.inputLogin.value, this.refs.password.value)} className="BtnSubmitLogin" variant="primary"><h6 className="textTitleBTNLogin pointer">Login</h6></Button>
                                        </div>
                                        <div className="novoCadastro">
                                            <h6 onClick={() => this.props.redirect("/formLogin")} className="textTitleLogin pointer">Ainda não possui cadastro ? Cadastre-se</h6>
                                        </div>

                                    </div>
                                </Col>
                                <Col xs={0} sm={0} sm={1} lg={1}>

                                    <div className="line"></div>
                                </Col>
                                <Col xs={12} sm={12} sm={5} lg={5}>
                                    <div className="right">
                                        <div className="qRCodeText"><h6 className="textTitleLogin">Acesse via QR Code - Autenticação pela Wallet.</h6></div>
                                        <img className="qrCode" alt="" src={imgPrincipal} />
                                    </div>
                                </Col>
                            </Row>
                            <div className="controlMarginLogin"></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        LoginComponent: state.LoginComponent.LoginComponent,
        errorLogin: state.errorLogin.errorLogin,
        sucessLogin: state.sucessLogin.sucessLogin,
        StyleHeader: state.StyleHeader.StyleHeader
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ login, changeLoginData, redirect, setErrorData, setSucessData, setHeader }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)