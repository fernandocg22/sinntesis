import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import ConsultarReducer from './components/consultar/consultarReduces'
import EditalReducer from './components/edital/editalReduces'
import RepresentacaoReducer from './components/representacao/representacaoReduces'
import DecisaoReducer from './components/decisao/deicsaoReduces'
import VotoReducer from './components/voto/votoReduces'
import LoginReducer from './components/login/loginReducer'
import TermoReducer from './components/termo/termoReducers'
import history from './routes/history'
import headerReducer from './components/header/headerReducer'



const rootReducer = combineReducers({
    router: connectRouter(history),
    //Header 
    StyleHeader: headerReducer, 
    //Login
    LoginComponent:LoginReducer,
    sucessLogin: LoginReducer,
    errorLogin: LoginReducer,
    //Termos 
    listaTermos: TermoReducer,
    listaTermosAdicionados: TermoReducer,
    inputPalavraChavePopup: TermoReducer,
    inputDescricaoPopup: TermoReducer,
    

    //consultar
    tipo: ConsultarReducer,
    document: ConsultarReducer,
    tipo_selecionado: ConsultarReducer,
    proximidade:ConsultarReducer,
    document_selecionado: ConsultarReducer,
    files: ConsultarReducer,
    editalSelecionado: ConsultarReducer,
    listaRepresentacoes: ConsultarReducer,
    listaEditaisProximos: ConsultarReducer,
    clickedIndex: ConsultarReducer,
    wordCloud: ConsultarReducer,
    consultarSucess: ConsultarReducer,
    consultarError: ConsultarReducer,
    listaEditaisProximosPC: ConsultarReducer,
    handleShow: ConsultarReducer,
    consultaValue: ConsultarReducer,

    //Edital
    Edital_tipo: EditalReducer,
    Edital_tipo_selecionado: EditalReducer,
    Edital_pessoa_juridica: EditalReducer,
    Edital_pessoa_juridica_selecionada: EditalReducer,
    Edital_processo: EditalReducer,
    Edital_Descricao: EditalReducer,
    Edital_files: EditalReducer,
    Edital_pond: EditalReducer,
    Edital_sucess: EditalReducer,
    isOn:EditalReducer,
    Edital_palavras_chaves: EditalReducer,
    Edital_Titulo: EditalReducer,
    loading: EditalReducer,
    sucessEdital: EditalReducer,
    errorEdital: EditalReducer,
    editalView: EditalReducer,
    warningEdital: EditalReducer,

    //Representacao
    Representacao_tipo: RepresentacaoReducer,
    Representacao_pessoa_juridica: RepresentacaoReducer,
    Representacao_pessoa_juridica_selecionada: RepresentacaoReducer,
    Representacao_processo: RepresentacaoReducer,
    Representacao_Descricao: RepresentacaoReducer,
    Representacao_files: RepresentacaoReducer,
    Representacao_pond: RepresentacaoReducer,
    Representacao_sucess: RepresentacaoReducer,
    Representacao_tipo_selecionado: RepresentacaoReducer,
    Representacao_palavras_chaves: RepresentacaoReducer,
    Representacao_titulo: RepresentacaoReducer,
    Representacao_edital_selecionado: RepresentacaoReducer,
    Representacao_edital: RepresentacaoReducer,
    Representacao_loading: RepresentacaoReducer,
    sucessRepresentacao: RepresentacaoReducer,
    errorRepresentacao: RepresentacaoReducer,
    warningRepresentacao: RepresentacaoReducer,
    Representacao_lei_selecionado: RepresentacaoReducer,
    Representacao_leis: RepresentacaoReducer,
    Representacao_leis_input: RepresentacaoReducer,



    //Decisao
    Decisao_tipo: DecisaoReducer,
    Decisao_tipo_selecionado: DecisaoReducer,
    Decisao_pessoa_juridica: DecisaoReducer,
    Decisao_pessoa_juridica_selecionada: DecisaoReducer,
    Decisao_processo: DecisaoReducer,
    Decisao_Descricao: DecisaoReducer,
    Decisao_files: DecisaoReducer,
    Decisao_pond: DecisaoReducer,
    Decisao_sucess: DecisaoReducer,
    Decisao_palavras_chaves: DecisaoReducer,
    Decisao_representacao_data: DecisaoReducer,
    Decisao_representacao_selecionada: DecisaoReducer,
    Decisao_titulo: DecisaoReducer,
    loading_decisao: DecisaoReducer,
    sucessDecisao: DecisaoReducer,
    errorDecisao: DecisaoReducer,
    Decisao_Texto: DecisaoReducer,
    generatePdf: DecisaoReducer,
    warningDecisao: DecisaoReducer,

    //Voto
    Voto_tipo: VotoReducer,
    Voto_tipo_selecionado: VotoReducer,
    Voto_pessoa_juridica: VotoReducer,
    Voto_pessoa_juridica_selecionada: VotoReducer,
    Voto_processo: VotoReducer,
    Voto_Descricao: VotoReducer,
    Voto_files: VotoReducer,
    Voto_pond: VotoReducer,
    Voto_sucess: VotoReducer,
})

export default rootReducer