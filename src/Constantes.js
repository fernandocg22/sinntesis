export const getUrl = (base) => {
        var base_url =''
        var base_url_training =''
        var url_file_server =''
    if (base == '187.102.147.18') {
        base_url = "http://" + base + ":8080/"
        base_url_training = "http://" + base + ":5000/"
        url_file_server = "http://" + base + ":8400/files/"
    }
    else {
        base_url = 'https://sinntesis.sinn.solutions/api/';
        //base_url = 'http://localhost:9090/';
        base_url_training = 'http://192.168.10.207:5000/';
        url_file_server = 'https://sinntesis.sinn.solutions/api/files/';

    }

    return {base_url,base_url_training,url_file_server}
}
/*

export const getUrl = (base) => {
    var base_url =''
    var base_url_training =''
    var url_file_server =''
if (base == '187.102.147.18') {
    base_url = "http://" + base + ":8080/documentos/"
    base_url_training = "http://" + base + ":5000/"
    url_file_server = "http://" + base + ":8080/documentos/files/"
}
else {
    base_url = 'http://10.26.0.195:8080/documentos/';
    base_url_training = 'http://10.26.0.195:5000/';
    url_file_server = 'http://10.26.0.195:8080/documentos/files/';

}

return {base_url,base_url_training,url_file_server}
}
*/
