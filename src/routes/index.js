import React from "react";
import { Switch, Route } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";
import { Container } from 'react-bootstrap';

import Panel from "../components/panel/panel";
import Edital from "../components/edital/edital"
import Representacao from "../components/representacao/representacao"
import Decisao from "../components/decisao/decisao"
import panelEdital from "../components/panel/panelEdital";
import Consultar from "../components/consultar/consultar"
import ConsultarRepresentacao from "../components/consultar/consultarRepresentacao"
import Header from "../components/header/header"
import panelInicial from "../components/panel/panelInicial"
import Login from "../components/login/login"
import Termos from '../components/termo/termo'
import Leis from '../components/leis/leis'
import Footer from '../components/footer/footer'
import EditarTextoDecisao from '../components/decisao/editarTextoDecisao'
import FormLogin from "../components/login/formlogin"
import history from "./history";
import axios from 'axios';


axios.interceptors.request.use(function (config) {
  console.log(config)

  if (JSON.parse(window.sessionStorage.getItem('dataUsers')) && !config.url.includes('5000'))
    config.headers.Authorization = "	Bearer " + JSON.parse(window.sessionStorage.getItem('dataUsers')).token
  return config;
}, function (err) {
  return Promise.reject(err);
});



axios.interceptors.response.use((response) => response, (error) => {
  let value = error.response;
  console.log(error)
  if (value.status === 401 && value.data.message === 'Expired JWT Token'
    && (!value.config || !value.config.renewToken)) {
    console.log('Token JWT expiré. Reconnexion ...');
    // renewToken performs authentication using username/password saved in sessionStorage/window.sessionStorage
    return this.renewToken().then(() => {
      error.config.baseURL = undefined; // baseURL is already present in url
      return this.axios.request(error.config);
    }).then((response) => {
      console.log('Reconnecté !');
      return response;
    });

  } else if (value.status === 401 && value.config && value.config.renewToken) {
    console.log('Echec de la reconnexion !');

    if (error.message) {
      error.message = 'Echec de la reconnexion ! ' + error.message + '.';
    } else {
      error.message = 'Echec de la reconnexion !';
    }

  } else if (value.status === 401) {
    console.log('Accès refusé.');
    window.sessionStorage.clear()
    history.push("/")
    // TODO: We could ask for username/password in a modal dialog...
  }

  return Promise.reject(error);
});


const Routes = () => (
  <div >
    <Container>
      <Header />
      <ConnectedRouter history={history}>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/leis" component={Leis} /> 
          <Route path="/panelinicial" component={panelInicial} />
          <Route path="/paneledital" component={panelEdital} />
          <Route path="/textoDecisao" component={EditarTextoDecisao}/>
          <Route path="/edital" component={Edital} />
          <Route path="/termos" component={Termos} /> 
          <Route path="/representacao" component={Representacao} />
          <Route path="/decisao" component={Decisao} />
          <Route path="/lista" component={Consultar} />
          <Route path="/consultarRepresentacao" component={ConsultarRepresentacao} />
          <Route path="/panel" component={Panel} />
          <Route path="/login" component={Login} />
          <Route path="/formLogin" component={FormLogin} />
        </Switch>
      </ConnectedRouter>
      <Footer/>
    </Container>
  </div>
);

export default Routes